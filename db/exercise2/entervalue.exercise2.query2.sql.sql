2. WRITE A QUERY TO INSERT AT LEAST 5 EMPLOYEES FOR EACH DEPARTMENT

INSERT INTO `employee`.`department` (`department_id`, `department_name`) VALUES ('100', 'ITDesk');
INSERT INTO `employee`.`department` (`department_id`, `department_name`) VALUES ('101', 'Finance');
INSERT INTO `employee`.`department` (`department_id`, `department_name`) VALUES ('102', 'Engineering');
INSERT INTO `employee`.`department` (`department_id`, `department_name`) VALUES ('103', 'HR');
INSERT INTO `employee`.`department` (`department_id`, `department_name`) VALUES ('104', 'Recruitment');
INSERT INTO `employee`.`department` (`department_id`, `department_name`) VALUES ('105', 'Facility');

INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('A1', 'Nithisree', 'b', '2000-06-15', '2022-01-02', '1500000', '100');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('B1', 'Sheela', 'sharma', '1999-12-15', '2021-03-06', '800000', '100');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('C1', 'Peter', 'Kavinsky', '2000-01-30', '2022-05-07', '1000000', '100');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('D1', 'Noah', 'Flynl', '1998-07-08', '2021-04-09', '5000000', '100');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('E1', 'John', 'A', '1972-12-31', '2001-01-19', '1200000', '100');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('A2', 'Mathumitha', 'C', '2001-03-07', '2022-08-12', '2500000', '101');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('B2', 'Leela', 'Krishnan', '1976-05-05', '2002-12-01', '700000', '101');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('C2', 'Lishanthini', 'Vel', '2000-12-08', '2021-05-02', '9000000', '101');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('D2', 'Kiara', 'Adhvani', '1989-02-20', '2016-02-21', '400000', '101');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('E2', 'Pooja ', 'Sivasubramaniam', '2000-09-06', '2020-03-17', '3000000', '101');

INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('A3', 'Megamalya', 'T', '2000-07-06', '2020-04-04', '3500000', '102');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('B3', 'Advik', 'J', '2000-02-04', '2022-07-22', '1500000', '102');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('C3', 'Sachin', 'T', '1980-03-05', '2000-05-09', '600000', '102');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('D3', 'Srija', 'Sivakumar', '1992-04-10', '2016-09-10', '800000', '102');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('E3', 'Mithra', '1993-10-15', '2018-10-01', '700000', '102');

INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('A4', 'Arun', 'kumar', '1985-02-21', '2017-11-05', '300000', '103');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('B4', 'Dhivya', 'M', '1972-09-09', '2008-06-19', '120000', '103');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('C4', 'SriDivya', '1985-01-05', '2009-02-15', '150000', '103');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('D4', 'Charles', 'babage', '1986-05-07', '2010-05-30', '200000', '103');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('E4', 'Venu', '1972-03-02', '2005-09-09', '300000', '103');

INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('A5', 'Gayathri', 'jayaram', '1965-08-07', '2003-05-07', '100000', '104');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('B5', 'Meera', 'Mithun', '1963-01-17', '2001-02-21', '200000', '104');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('C5', 'Julie', '1986-11-11', '2006-02-19', '30000', '104');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('D5', 'Deepa', 'Vijay', '1974-01-06', '1999-12-12', '200000', '104');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('E5', 'Vanitha', 'Paul', '1974-02-13', '2000-11-13', '500000', '104');

INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('A6', 'Elizabeth', 'Peter', '1974-05-06', '2001-04-15', '700000', '105');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('B6', 'Cheran', '1979-09-25', '2003-05-09', '500000', '105');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('C6', 'Kasthuri', 'Bhai', '1989-02-07', '2005-09-08', '200000', '105');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('D6', 'Jhansi', 'Rani', '1994-05-26', '2010-03-29', '700000', '105');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('E6', 'lakshmi', 'Ram', '1989-02-06', '2007-05-31', '400000', '105');
