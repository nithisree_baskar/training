10. Prepare a query to find out fresher(no department allocated employee) in the employee, where no matching records in the department table.


INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `area`) VALUES ('F1', 'Sanjay', 'Ramasamy', '2000-05-03', '2022-11-10', '500000', 'Salem');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `dob`, `date_of_joining`, `annual_salary`, `area`) VALUES ('F2', 'Mumtaz', '1995-05-06', '2017-04-16', '800000', 'Sivagangai');

SELECT emp_id
      ,first_name
	  ,surname
	  ,dob
	  ,date_of_joining
	  ,area 
	  from employee 
	  where department_id is NULL;