7. Prepare an example for self-join

SELECT a.emp_id
      ,a.first_name
	  ,b.department_id 
	  from employee a
	  ,employee b 
	  where a.department_id=b.department_id 
	  and b.emp_id="A1";
	  
	  
Explanation:
Two seperate copies of the same table employee is created for aliases a and b.
This query will return first_name and emp_id of the emp_id A1.Since A1's department_id is 100.