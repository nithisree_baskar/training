11. Write a query to get employee names and their respective department name

SELECT employee.first_name
      ,department.department_name 
	  FROM employee 
	  INNER JOIN department  
	  where department.department_id=employee.department_id;
