8. Write a query to list out employees from the same area, and from the same department


ALTER TABLE `employee`.`employee` 
ADD COLUMN `area` VARCHAR(45) NOT NULL AFTER `department_id`;

UPDATE `employee`.`employee` SET `area` = 'coimbatore' WHERE (`emp_id` = 'A1');
UPDATE `employee`.`employee` SET `area` = 'trichy' WHERE (`emp_id` = 'A2');
UPDATE `employee`.`employee` SET `area` = 'salem' WHERE (`emp_id` = 'A3');
UPDATE `employee`.`employee` SET `area` = 'erode' WHERE (`emp_id` = 'A4');
UPDATE `employee`.`employee` SET `area` = 'tirupur' WHERE (`emp_id` = 'A5');
UPDATE `employee`.`employee` SET `area` = 'theni' WHERE (`emp_id` = 'A6');
UPDATE `employee`.`employee` SET `area` = 'ariyalur' WHERE (`emp_id` = 'B1');
UPDATE `employee`.`employee` SET `area` = 'dindugal' WHERE (`emp_id` = 'B2');
UPDATE `employee`.`employee` SET `area` = 'sivagangai' WHERE (`emp_id` = 'B3');
UPDATE `employee`.`employee` SET `area` = 'thiruvarur' WHERE (`emp_id` = 'B4');
UPDATE `employee`.`employee` SET `area` = 'chennai' WHERE (`emp_id` = 'B5');
UPDATE `employee`.`employee` SET `area` = 'thoothukudi' WHERE (`emp_id` = 'B6');
UPDATE `employee`.`employee` SET `area` = 'madurai' WHERE (`emp_id` = 'C1');
UPDATE `employee`.`employee` SET `area` = 'kanyakumari' WHERE (`emp_id` = 'C2');
UPDATE `employee`.`employee` SET `area` = 'virudhunagar' WHERE (`emp_id` = 'C3');
UPDATE `employee`.`employee` SET `area` = 'vilupuram' WHERE (`emp_id` = 'C4');
UPDATE `employee`.`employee` SET `area` = 'vellore' WHERE (`emp_id` = 'C5');
UPDATE `employee`.`employee` SET `area` = 'ooty' WHERE (`emp_id` = 'C6');
UPDATE `employee`.`employee` SET `area` = 'pazhani' WHERE (`emp_id` = 'D1');
UPDATE `employee`.`employee` SET `area` = 'kodaikanal' WHERE (`emp_id` = 'D2');
UPDATE `employee`.`employee` SET `area` = 'thirupathi' WHERE (`emp_id` = 'D3');
UPDATE `employee`.`employee` SET `area` = 'theni' WHERE (`emp_id` = 'D4');
UPDATE `employee`.`employee` SET `area` = 'coimbatore' WHERE (`emp_id` = 'D5');
UPDATE `employee`.`employee` SET `area` = 'thoothukudi' WHERE (`emp_id` = 'D6');
UPDATE `employee`.`employee` SET `area` = 'namakkal' WHERE (`emp_id` = 'E1');
UPDATE `employee`.`employee` SET `area` = 'nagapattinam' WHERE (`emp_id` = 'E2');
UPDATE `employee`.`employee` SET `area` = 'bangalore' WHERE (`emp_id` = 'E3');
UPDATE `employee`.`employee` SET `area` = 'hosur' WHERE (`emp_id` = 'E4');
UPDATE `employee`.`employee` SET `area` = 'sengalpattu' WHERE (`emp_id` = 'E5');
UPDATE `employee`.`employee` SET `area` = 'karur' WHERE (`emp_id` = 'E6');
UPDATE `employee`.`employee` SET `area` = 'coimbatore' WHERE (`emp_id` = 'B1');
UPDATE `employee`.`employee` SET `area` = 'trichy' WHERE (`emp_id` = 'B2');
UPDATE `employee`.`employee` SET `area` = 'trichy' WHERE (`emp_id` = 'C2');
UPDATE `employee`.`employee` SET `area` = 'salem' WHERE (`emp_id` = 'B3');
UPDATE `employee`.`employee` SET `area` = 'erode' WHERE (`emp_id` = 'B4');
UPDATE `employee`.`employee` SET `area` = 'chennai' WHERE (`emp_id` = 'A5');
UPDATE `employee`.`employee` SET `area` = 'chennai' WHERE (`emp_id` = 'C5');
UPDATE `employee`.`employee` SET `area` = 'theni' WHERE (`emp_id` = 'B6');
UPDATE `employee`.`employee` SET `area` = 'theni' WHERE (`emp_id` = 'C6');

SELECT department_id
     ,first_name
	 ,surname
	 ,area 
	 from employee where area="coimbatore" and department_id="100";
SELECT department_id
     ,first_name
	 ,surname
	 ,area 
	 from employee where area="trichy" and department_id="101";
SELECT department_id
     ,first_name
	 ,surname
	 ,area
	 from employee where area="erode" and department_id="103";
SELECT department_id
     ,first_name
	 ,surname
	 ,area 
	 from employee where area="salem" and department_id="102";
SELECT department_id
     ,first_name
	 ,surname
	 ,area 
	 from employee where area="chennai" and department_id="104";
SELECT department_id
     ,first_name
	 ,surname
	 ,area 
	 from employee where area="theni" and department_id="105";




