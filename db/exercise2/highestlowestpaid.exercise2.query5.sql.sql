5. Find out the highest and least paid in all the department

SELECT department_id
     , MAX(annual_salary) 
	   from employee group by department_id;

SELECT department_id
     , MIN(annual_salary) 
	   from employee group by department_id;
	   