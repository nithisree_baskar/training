13.At-least use 3 tables for Join
Union 2 Different table with same Column name (use Distinct,ALL)

ALTER TABLE `school`.`faculty` 
ADD COLUMN `transport_id` INT NULL AFTER `age`;
ALTER TABLE `school`.`class` 
DROP COLUMN `student_id`,
DROP PRIMARY KEY;
;
ALTER TABLE `school`.`class` 
ADD COLUMN `class_id` VARCHAR(45) NOT NULL AFTER `class`,
ADD PRIMARY KEY (`class_id`);
;
INSERT INTO `school`.`class` (`student_name`, `class`, `class_id`) VALUES ('Varsha', '9', '9A');
INSERT INTO `school`.`class` (`student_name`, `class`, `class_id`) VALUES ('Janani', '2', '2C');
INSERT INTO `school`.`class` (`student_name`, `class`, `class_id`) VALUES ('Akila', '4', '4B');
INSERT INTO `school`.`class` (`student_name`, `class`, `class_id`) VALUES ('Pavithra', '5', '5A');
INSERT INTO `school`.`class` (`student_name`, `class`, `class_id`) VALUES ('Dharshana', '10', '10C');

INSERT INTO `school`.`class` (`student_name`, `class`, `class_id`) VALUES ('keerthana', '10', '10B');
INSERT INTO `school`.`class` (`student_name`, `class`, `class_id`) VALUES ('iniya', '10', '10A');
INSERT INTO `school`.`class` (`student_name`, `class`, `class_id`) VALUES ('Thiranya', '7', '7B');
INSERT INTO `school`.`class` (`student_name`, `class`, `class_id`) VALUES ('Aparna', '3', '3A');
INSERT INTO `school`.`class` (`student_name`, `class`, `class_id`) VALUES ('sona', '8', '8B');

select student_name
      ,class 
	  from student 
	  union select student_name
	  ,class 
	  from class;
	  
ALTER TABLE `school`.`faculty` 
ADD COLUMN `transport_id` INT NOT NULL AFTER `age`;
UPDATE `school`.`faculty` SET `transport_id` = '1' WHERE (`faculty_id` = 'FA1');
UPDATE `school`.`faculty` SET `transport_id` = '2' WHERE (`faculty_id` = 'FA2');
UPDATE `school`.`faculty` SET `transport_id` = '3' WHERE (`faculty_id` = 'FA3');

SELECT student.student_id,student.student_name,faculty.transport_id,transport.transport_mode,transport.transport_id 
from student
inner join faculty on faculty.transport_id=student.transport_id 
inner join transport on faculty.transport_id=transport.transport_id
order by faculty.transport_id asc; 