8.Fetch ALL records and Fetch particular column Records

INSERT INTO `school`.`student` (`student_id`, `student_name`, `class`, `gender`, `age`) VALUES ('18ec088', 'Megamalya', '10', 'F', '16');
INSERT INTO `school`.`student` (`student_id`, `student_name`, `class`, `gender`, `age`) VALUES ('18ec054', 'Jessie', '9', 'F', '15');
INSERT INTO `school`.`student` (`student_id`, `student_name`, `class`, `gender`, `age`) VALUES ('18ec076', 'Harish', '8', 'M', '13');
INSERT INTO `school`.`student` (`student_id`, `student_name`, `class`, `gender`, `age`) VALUES ('18ec001', 'Krithik', '9', 'M', '14');
INSERT INTO `school`.`student` (`student_id`, `student_name`, `class`, `gender`, `age`) VALUES ('18ec091', 'Noah', '10', 'M', '16');
INSERT INTO `school`.`student` (`student_id`, `student_name`, `class`, `gender`, `age`) VALUES ('18ec081', 'Lara', '11', 'F', '17');
INSERT INTO `school`.`student` (`student_id`, `student_name`, `class`, `gender`, `age`) VALUES ('18ec099', 'Nairobi', '12', 'F', '18');
INSERT INTO `school`.`student` (`student_id`, `student_name`, `class`, `gender`, `age`) VALUES ('18ec055', 'Rio', '8', 'M', '13');

SELECT *from student;

SELECT student_id
      ,student_name 
	  from student 
	  where student_id="18ec072";
