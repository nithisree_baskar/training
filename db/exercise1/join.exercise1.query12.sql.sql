12.Read Records by Join using tables FOREIGN KEY relationships (CROSS, INNER, LEFT, RIGHT)

select student.student_id
      ,student.student_id
	  ,transport.transport_id
	  ,transport.transport_mode 
	  from student 
	  inner join transport on student.transport_id=transport.transport_id;

select student.student_name
      ,transport.transport_mode 
	  from student 
	  full join transport on student.transport_id=transport.transport_id;
	  
select student.student_name
      ,transport.transport_mode 
	  from student 
	  left join transport on student.transport_id=transport.transport_id;
	  
select student.student_name
      ,transport.transport_mode 
	  from student 
	  right join transport on student.transport_id=transport.transport_id;
	  
select *from student 
	  cross join transport;
	  