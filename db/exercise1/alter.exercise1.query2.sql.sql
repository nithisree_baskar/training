2.Alter Table with Add new Column and Modify\Rename\Drop column

ALTER TABLE `school`.`student` 
ADD COLUMN `age` INT NOT NULL AFTER `gender`,
ADD COLUMN `location` VARCHAR(45) NOT NULL AFTER `age`;

ALTER TABLE `school`.`student` 
DROP COLUMN `location`;