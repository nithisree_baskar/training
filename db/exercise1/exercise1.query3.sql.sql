3.Use Constraints NOT NULL, DEFAULT, CHECK, PRIMARY KEY, FOREIGN KEY, KEY, INDEX

CREATE TABLE `school`.`office` (
  `full_name` VARCHAR(45) NOT NULL,
  `area` VARCHAR(20) NOT NULL,
  `transport` VARCHAR(45) NOT NULL);
  
  ALTER TABLE `school`.`office` 
ADD COLUMN `transport_id` INT NOT NULL AFTER `transport`;

ALTER table Student ADD CHECK(marks > 0);

ALTER TABLE `school`.`office` 
ADD COLUMN `bus_id` VARCHAR(45) NOT NULL AFTER `transport_id`,
ADD UNIQUE INDEX `bus_id_UNIQUE` (`bus_id` ASC);

ALTER TABLE `school`.`student` 
ADD COLUMN `transport_id` VARCHAR(45) NOT NULL AFTER `grade`;

ALTER TABLE `school`.`office` 
ADD PRIMARY KEY (`transport_id`);
;

CREATE INDEX idx_age
   ON student (age);
   
ALTER TABLE `school`.`student` 
ADD CONSTRAINT `transport_id_transport`
  FOREIGN KEY (`transport_id`)
  REFERENCES `school`.`transport` (`transport_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
   
   