9.USE Where to filter records using AND,OR,NOT,LIKE,IN,ANY, wildcards ( % _ )

Select student_id
      ,student_name 
	  from student 
	  where class="9" and gender="F";
	  

select student_id
      ,student_name 
	  from student 
	  where class="9" or age="13";

select student_id
      ,student_name 
	  from student 
	  where not class= "9";

select student_id
      ,student_name 
	  from student 
	  where class like 9;

select student_id
      ,student_name 
	  from student 
	  where class in (10,11);

select student_id
      ,student_name 
	  from student 
	  where student_id=any( select class from student where class="9");

SELECT student_name
      ,student_id
	  ,gender
	  ,age 
	  FROM student 
	  WHERE student_name LIKE '%a' ;