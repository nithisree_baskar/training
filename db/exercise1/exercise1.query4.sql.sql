4.Truncate Records From table

CREATE TABLE `school`.`faculty` (
  `faculty_id` VARCHAR(20) NOT NULL,
  `faculty_name` VARCHAR(45) NOT NULL,
  `age` INT NULL,
  PRIMARY KEY (`faculty_id`));
  
INSERT INTO `school`.`faculty` (`faculty_id`, `faculty_name`, `age`) VALUES ('FA1', 'Annie', '35');
INSERT INTO `school`.`faculty` (`faculty_id`, `faculty_name`, `age`) VALUES ('FA2', 'Geetha', '40');
INSERT INTO `school`.`faculty` (`faculty_id`, `faculty_name`, `age`) VALUES ('FA3', 'Harish', '29');

alter table faculty drop column faculty_name;

