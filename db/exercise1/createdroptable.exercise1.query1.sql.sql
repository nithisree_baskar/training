1.Create Database\table and Rename\Drop Table

create table student(student_id varchar(20) NOT NULL primary key, student_name varchar(45) NOT NULL, class varchar(10), gender char);
ALTER TABLE `school`.`student` 
CHANGE COLUMN `gender` `gender` CHAR(1) NOT NULL 

CREATE TABLE `school`.`staff` (
  `staff_id` INT NOT NULL,
  `staff_name` VARCHAR(45) NOT NULL,
  `age` INT NOT NULL,
  PRIMARY KEY (`staff_id`));

drop table staff;