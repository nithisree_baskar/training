10.USE inbuilt Functions - now,MAX, MIN, AVG, COUNT, FIRST, LAST, SUM

ALTER TABLE `school`.`student` 
ADD COLUMN `marks` INT NOT NULL AFTER `age`,
ADD COLUMN `grade` CHAR(1) NOT NULL AFTER `marks`;

UPDATE `school`.`student` SET `marks` = '70', `grade` = 'B' WHERE (`student_id` = '18ec001');
UPDATE `school`.`student` SET `marks` = '75', `grade` = 'B' WHERE (`student_id` = '18ec054');
UPDATE `school`.`student` SET `marks` = '80', `grade` = 'A' WHERE (`student_id` = '18ec055');
UPDATE `school`.`student` SET `marks` = '50', `grade` = 'C' WHERE (`student_id` = '18ec072');
UPDATE `school`.`student` SET `marks` = '95', `grade` = 'O' WHERE (`student_id` = '18ec073');
UPDATE `school`.`student` SET `marks` = '65', `grade` = 'B' WHERE (`student_id` = '18ec076');
UPDATE `school`.`student` SET `marks` = '55', `grade` = 'C' WHERE (`student_id` = '18ec081');
UPDATE `school`.`student` SET `marks` = '85', `grade` = 'A' WHERE (`student_id` = '18ec088');
UPDATE `school`.`student` SET `marks` = '35', `grade` = 'E' WHERE (`student_id` = '18ec091');
UPDATE `school`.`student` SET `marks` = '75', `grade` = 'B' WHERE (`student_id` = '18ec099');

select student_id,student_name,now() as result from student;

select student_id,student_name,class,max(marks) as highest from student;

select student_id,student_name,class,min(age) as youngest from student;

select avg(marks) as average from student;

select count(student_id) as class_strength from student;

select sum(marks) as total from student;


