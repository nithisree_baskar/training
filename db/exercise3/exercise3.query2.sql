2. Select final year students(Assume that universities has Engineering Depts only) details who are studying under a particular university and selected cities alone.
ROLL_NUMBER, NAME, GENDER, DOB, EMAIL, PHONE, ADDRESS,
COLLEGE_NAME, DEPARTMENT_NAME


SELECT student.`roll_number`
	  ,student.`name`
      ,student.`gender`
      ,student.`dob`
      ,student.`email`
      ,student.`phone`
      ,student.`address`
      ,student.`academic_year` AS 'Batch'
      ,college.`name`
      ,department.`dept_name`
  FROM department,student,college_department,college,university
 WHERE university.univ_code = college.univ_code
   AND student.coll_id = college.id
   AND student.cdep_id = college_department.cdept_id
   AND college_department.udept_code = department.dept_code
   AND university.university_name = 'vit'
   AND college.city = 'Coimbatore'
   AND student.academic_year = 2022