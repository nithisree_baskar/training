7. Update PAID_STATUS and PAID_YEAR in SEMESTER_FEE table who has done the payment. Entries should be updated based on student ROLL_NUMBER.
a) Single Update - Update one student entry
b) Bulk Update - Update multiple students entries
c) PAID_STATUS value as ‘Paid’
d) PAID_YEAR value as ‘year format’


UPDATE semester_fee  
SET    paid_year = (2020)
WHERE  stud_id = 1001;
UPDATE semester_fee  
SET    paid_status = 'pending'
WHERE  stud_id = 1004; 

UPDATE semester_fee
SET    paid_status =
  (case stud_id when 1003 then 'paid'
                when 1004 then 'paid'
                when 1008 then 'paid'
    end)
WHERE  stud_id IN(1003,1004,1008);
UPDATE semester_fee
SET    paid_year =
  (case stud_id when 1005 then (2020)
                when 1006 then (2020)
                
    end)
WHERE stud_id IN(1005,1006); 
SELECT * FROM semester_fee;