CREATE SCHEMA `university` ;

CREATE TABLE `university`.`university` (
  `univ_code` CHAR(4) NOT NULL,
  `university_name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`univ_code`));

CREATE TABLE `university`.`designation` (
  `id` INT NOT NULL,
  `name` VARCHAR(30) NOT NULL,
  `rank` CHAR(1) NOT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `university`.`college` (
  `id` INT NOT NULL,
  `code` CHAR(4) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `univ_code` CHAR(4) NULL,
  `city` VARCHAR(50) NOT NULL,
  `state` VARCHAR(50) NOT NULL,
  `year_opened` YEAR(4) NOT NULL,
  PRIMARY KEY (`id`));

ALTER TABLE `university`.`college` 
ADD INDEX `univ_code_university_idx` (`univ_code` ASC) VISIBLE;
;
ALTER TABLE `university`.`college` 
ADD CONSTRAINT `univ_code_university`
  FOREIGN KEY (`univ_code`)
  REFERENCES `university`.`university` (`univ_code`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

CREATE TABLE `university`.`department` (
  `dept_code` CHAR(4) NOT NULL,
  `dept_name` VARCHAR(50) NOT NULL,
  `univ_code` CHAR(4) NULL,
  PRIMARY KEY (`dept_code`));

ALTER TABLE `university`.`department` 
ADD INDEX `univ_code_college_idx` (`univ_code` ASC) VISIBLE;
;
ALTER TABLE `university`.`department` 
ADD CONSTRAINT `univ_code_college`
  FOREIGN KEY (`univ_code`)
  REFERENCES `university`.`university` (`univ_code`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

CREATE TABLE `university`.`employee` (
  `id` INT NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `dob` DATE NOT NULL,
  `email` VARCHAR(50) NOT NULL,
  `phone` BIGINT(12) NOT NULL,
  `college_id` INT NOT NULL,
  `cdept_id` INT NOT NULL,
  `desig_id` INT NOT NULL,
  PRIMARY KEY (`id`));
ALTER TABLE `university`.`employee` 
ADD INDEX `college_id_college_idx` (`college_id` ASC) VISIBLE;
;
ALTER TABLE `university`.`employee` 
ADD CONSTRAINT `college_id_college`
  FOREIGN KEY (`college_id`)
  REFERENCES `university`.`college` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
ALTER TABLE `university`.`employee` 
ADD INDEX `desig_id_designation_idx` (`desig_id` ASC) VISIBLE;
;
ALTER TABLE `university`.`employee` 
ADD CONSTRAINT `desig_id_designation`
  FOREIGN KEY (`desig_id`)
  REFERENCES `university`.`designation` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

CREATE TABLE `university`.`college_department` (
  `cdept_id` INT NOT NULL,
  `udept_code` CHAR(4) NOT NULL,
  `college_id` INT NOT NULL,
  PRIMARY KEY (`cdept_id`));

ALTER TABLE `university`.`college_department` 
ADD CONSTRAINT `collegeid_college_department`
  FOREIGN KEY (`collegeid`)
  REFERENCES `university`.`college` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
ALTER TABLE `university`.`employee` 
ADD INDEX `cdept_id_college_department_idx` (`cdept_id` ASC) VISIBLE;
;
ALTER TABLE `university`.`employee` 
ADD CONSTRAINT `cdept_id_college_department`
  FOREIGN KEY (`cdept_id`)
  REFERENCES `university`.`college_department` (`cdept_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
  
CREATE TABLE `university`.`syllabus` (
  `id` INT NOT NULL,
  `cdept_id` INT NOT NULL,
  `syllabus_code` CHAR(4) NOT NULL,
  `syllabus_name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`));
  ALTER TABLE `university`.`syllabus` 
CHANGE COLUMN `cdept_id` `cdeptid` INT NOT NULL ;
ALTER TABLE `university`.`syllabus` 
ADD CONSTRAINT `cdeptid_college_department`
  FOREIGN KEY (`cdeptid`)
  REFERENCES `university`.`college_department` (`cdept_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
CREATE TABLE `university`.`professor_syllabus` (
  `emp_id` INT NOT NULL,
  `syllabus_id` INT NOT NULL,
  `semester` TINYINT(5) NOT NULL,
  PRIMARY KEY (`emp_id`));
ALTER TABLE `university`.`professor_syllabus` 
DROP PRIMARY KEY;
;
ALTER TABLE `university`.`professor_syllabus` 
ADD INDEX `emp_id_employee_idx` (`emp_id` ASC) VISIBLE;
;
ALTER TABLE `university`.`professor_syllabus` 
ADD CONSTRAINT `emp_id_employee`
  FOREIGN KEY (`emp_id`)
  REFERENCES `university`.`employee` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
ALTER TABLE `university`.`professor_syllabus` 
ADD INDEX `syllabus_id_syllabus_idx` (`syllabus_id` ASC) VISIBLE;
;
ALTER TABLE `university`.`professor_syllabus` 
ADD CONSTRAINT `syllabus_id_syllabus`
  FOREIGN KEY (`syllabus_id`)
  REFERENCES `university`.`syllabus` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
  
CREATE TABLE `university`.`student` (
  `id` INT NOT NULL,
  `roll_number` CHAR(8) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `dob` DATE NOT NULL,
  `gender` CHAR(1) NOT NULL,
  `email` VARCHAR(50) NOT NULL,
  `phone` BIGINT(12) NOT NULL,
  `address` VARCHAR(200) NOT NULL,
  `academic_year` YEAR(4) NOT NULL,
  `cdept_id` INT NOT NULL,
  `college_id` INT NOT NULL,
  PRIMARY KEY (`id`));
ALTER TABLE `university`.`student` 
CHANGE COLUMN `cdept_id` `cdep_id` INT NOT NULL ;

ALTER TABLE `university`.`student` 
ADD CONSTRAINT `cdep_id_college_department`
  FOREIGN KEY (`cdep_id`)
  REFERENCES `university`.`college_department` (`cdept_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
ALTER TABLE `university`.`student` 
CHANGE COLUMN `college_id` `coll_id` INT NOT NULL ;
ALTER TABLE `university`.`student` 
ADD INDEX `coll_id_college_idx` (`coll_id` ASC) VISIBLE;
;
ALTER TABLE `university`.`student` 
ADD CONSTRAINT `coll_id_college`
  FOREIGN KEY (`coll_id`)
  REFERENCES `university`.`college` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
CREATE TABLE `university`.`semester_fee` (
  `cd_id` INT NOT NULL,
  `stud_id` INT NOT NULL,
  `semester` TINYINT(4) NOT NULL,
  `amount` DOUBLE(18,2) NULL,
  `paid_year` YEAR(4) NULL,
  `paid_status` VARCHAR(10) NOT NULL);
ALTER TABLE `university`.`semester_fee` 
ADD INDEX `cd_id_college_department_idx` (`cd_id` ASC) VISIBLE;
;
ALTER TABLE `university`.`semester_fee` 
ADD CONSTRAINT `cd_id_college_department`
  FOREIGN KEY (`cd_id`)
  REFERENCES `university`.`college_department` (`cdept_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
ALTER TABLE `university`.`semester_fee` 
ADD INDEX `stud_id_student_idx` (`stud_id` ASC) VISIBLE;
;
ALTER TABLE `university`.`semester_fee` 
ADD CONSTRAINT `stud_id_student`
  FOREIGN KEY (`stud_id`)
  REFERENCES `university`.`student` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

CREATE TABLE `university`.`semester_result` (
  `st_id` INT NOT NULL,
  `syllabus_id` INT NOT NULL,
  `semester` TINYINT(5) NOT NULL,
  `grade` VARCHAR(2) NOT NULL,
  `credits` FLOAT NOT NULL,
  `result_date` DATE NOT NULL);

ALTER TABLE `university`.`semester_result` 
ADD INDEX `st_id_student_idx` (`st_id` ASC) VISIBLE;
;
ALTER TABLE `university`.`semester_result` 
ADD CONSTRAINT `st_id_student`
  FOREIGN KEY (`st_id`)
  REFERENCES `university`.`student` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
ALTER TABLE `university`.`semester_result` 
CHANGE COLUMN `syllabus_id` `syll_id` INT NOT NULL ;

ALTER TABLE `university`.`semester_result` 
ADD CONSTRAINT `syll_id_syllabus`
  FOREIGN KEY (`syll_id`)
  REFERENCES `university`.`syllabus` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
  
  
INSERT INTO `university`.`university` (`univ_code`, `university_name`) VALUES ('ab', 'vit');
INSERT INTO `university`.`university` (`univ_code`, `university_name`) VALUES ('bc', 'amritha');
INSERT INTO `university`.`university` (`univ_code`, `university_name`) VALUES ('cd', 'srm');
INSERT INTO `university`.`university` (`univ_code`, `university_name`) VALUES ('de', 'nit');
INSERT INTO `university`.`university` (`univ_code`, `university_name`) VALUES ('ef', 'kct');
INSERT INTO `university`.`university` (`univ_code`, `university_name`) VALUES ('fg', 'madras');
INSERT INTO `university`.`university` (`univ_code`, `university_name`) VALUES ('gh', 'bharathiyar');
INSERT INTO `university`.`university` (`univ_code`, `university_name`) VALUES ('hi', 'thiyagaraja');
INSERT INTO `university`.`university` (`univ_code`, `university_name`) VALUES ('ij', 'manipal');
INSERT INTO `university`.`university` (`univ_code`, `university_name`) VALUES ('jk', 'karpagam');

INSERT INTO `university`.`designation` (`id`, `name`, `rank`) VALUES ('1', 'hod', 'F');
INSERT INTO `university`.`designation` (`id`, `name`, `rank`) VALUES ('2', 'professor', 'S');
INSERT INTO `university`.`designation` (`id`, `name`, `rank`) VALUES ('3', 'assistantprofessor', 'T');
INSERT INTO `university`.`designation` (`id`, `name`, `rank`) VALUES ('4', 'lecturer', 'O');
INSERT INTO `university`.`designation` (`id`, `name`, `rank`) VALUES ('5', 'labassistant', 'L');

INSERT INTO `university`.`college` (`id`, `code`, `name`, `univ_code`, `city`, `state`, `year_opened`) VALUES ('100', 'aaaa', 'psg', 'ab', 'coimbatore', 'tamilnadu', 1999);
INSERT INTO `university`.`college` (`id`, `code`, `name`, `univ_code`, `city`, `state`, `year_opened`) VALUES ('101', 'bbbb', 'cit', 'ab', 'coimbatore', 'tamilnadu', 2000);
INSERT INTO `university`.`college` (`id`, `code`, `name`, `univ_code`, `city`, `state`, `year_opened`) VALUES ('102', 'cccc', 'kct', 'ab', 'coimbatore', 'tamilnadu', 2000);
INSERT INTO `university`.`college` (`id`, `code`, `name`, `univ_code`, `city`, `state`, `year_opened`) VALUES ('103', 'dddd', 'krishna', 'ab', 'coimbatore', 'tamilnadu', 2003);
INSERT INTO `university`.`college` (`id`, `code`, `name`, `univ_code`, `city`, `state`, `year_opened`) VALUES ('104', 'eeee', 'ramakrishna', 'ab', 'coimbatore', 'tamilnadu', 2002);
INSERT INTO `university`.`college` (`id`, `code`, `name`, `univ_code`, `city`, `state`, `year_opened`) VALUES ('105', 'ffff', 'karpagam', 'ab', 'coimbatore', 'tamilnadu', 2001);
INSERT INTO `university`.`college` (`id`, `code`, `name`, `univ_code`, `city`, `state`, `year_opened`) VALUES ('106', 'gggg', 'kongu', 'ab', 'coimbatore', 'tamilnadu', 2000);
INSERT INTO `university`.`college` (`id`, `code`, `name`, `univ_code`, `city`, `state`, `year_opened`) VALUES ('107', 'hhhh', 'jhonson', 'ab', 'erode', 'tamilnadu', 2002);
INSERT INTO `university`.`college` (`id`, `code`, `name`, `univ_code`, `city`, `state`, `year_opened`) VALUES ('108', 'iiii', 'sona', 'ab', 'salem', 'tamilnadu', 2001);
INSERT INTO `university`.`college` (`id`, `code`, `name`, `univ_code`, `city`, `state`, `year_opened`) VALUES ('109', 'jjjj', 'sengunthar', 'ab', 'erode', 'tamilnadu', 1999);
INSERT INTO `university`.`college` (`id`, `code`, `name`, `univ_code`, `city`, `state`, `year_opened`) VALUES ('110', 'kkkk', 'aadithya', 'ab', 'chennai', 'tamilnadu', 1999);
INSERT INTO `university`.`college` (`id`, `code`, `name`, `univ_code`, `city`, `state`, `year_opened`) VALUES ('111', 'llll', 'sastra', 'ab', 'chennai', 'tamilnadu', 2001);
INSERT INTO `university`.`college` (`id`, `code`, `name`, `univ_code`, `city`, `state`, `year_opened`) VALUES ('112', 'mmmm', 'presidency', 'ab', 'chennai', 'tamilnadu', 1989);
INSERT INTO `university`.`college` (`id`, `code`, `name`, `univ_code`, `city`, `state`, `year_opened`) VALUES ('113', 'nnnn', 'paavai', 'ab', 'chennai', 'tamilnadu', 2005);
INSERT INTO `university`.`college` (`id`, `code`, `name`, `univ_code`, `city`, `state`, `year_opened`) VALUES ('114', 'oooo', 'hindusthan', 'ab', 'chennai', 'tamilnadu', 2000);
INSERT INTO `university`.`college` (`id`, `code`, `name`, `univ_code`, `city`, `state`, `year_opened`) VALUES ('115', 'pppp', 'vellamal', 'ab', 'chennai', 'tamilnadu', 2000);
INSERT INTO `university`.`college` (`id`, `code`, `name`, `univ_code`, `city`, `state`, `year_opened`) VALUES ('116', 'qqqq', 'kongunaadu', 'ab', 'coimbatore', 'tamilnadu', 2005);
INSERT INTO `university`.`college` (`id`, `code`, `name`, `univ_code`, `city`, `state`, `year_opened`) VALUES ('117', 'rrrr', 'sethu', 'ab', 'madurai', 'tamilnadu', 2006);
INSERT INTO `university`.`college` (`id`, `code`, `name`, `univ_code`, `city`, `state`, `year_opened`) VALUES ('118', 'ssss', 'ranganathar', 'ab', 'coimbatore', 'tamilnadu', 1999);
INSERT INTO `university`.`college` (`id`, `code`, `name`, `univ_code`, `city`, `state`, `year_opened`) VALUES ('119', 'tttt', 'thiyagaraja', 'ab', 'madurai', 'tamilnadu', 1989);
INSERT INTO `university`.`college` (`id`, `code`, `name`, `univ_code`, `city`, `state`, `year_opened`) VALUES ('200', 'abab', 'manipal', 'bc', 'manipal', 'karnataka', 2007);
INSERT INTO `university`.`college` (`id`, `code`, `name`, `univ_code`, `city`, `state`, `year_opened`) VALUES ('201', 'bcbc', 'mahalingam', 'bc', 'pollachi', 'tamilnadu', 2006);
INSERT INTO `university`.`college` (`id`, `code`, `name`, `univ_code`, `city`, `state`, `year_opened`) VALUES ('300', 'aabb', 'panimalar', 'cd', 'chennai', 'tamilnadu', 2008);
INSERT INTO `university`.`college` (`id`, `code`, `name`, `univ_code`, `city`, `state`, `year_opened`) VALUES ('301', 'ccdd', 'christ', 'cd', 'bangalore', 'karnataka', 2000);

INSERT INTO `university`.`department` (`dept_code`, `dept_name`, `univ_code`) VALUES ('ece', 'electronics and communication', 'ab');
INSERT INTO `university`.`department` (`dept_code`, `dept_name`, `univ_code`) VALUES ('eee', 'electricals and electronics ', 'ab');
INSERT INTO `university`.`department` (`dept_code`, `dept_name`, `univ_code`) VALUES ('csc', 'computer science', 'ab');
INSERT INTO `university`.`department` (`dept_code`, `dept_name`, `univ_code`) VALUES ('it', 'information technology', 'ab');
INSERT INTO `university`.`department` (`dept_code`, `dept_name`, `univ_code`) VALUES ('mech', 'mechanical ', 'ab');
INSERT INTO `university`.`department` (`dept_code`, `dept_name`, `univ_code`) VALUES ('hist', 'history', 'bc');
INSERT INTO `university`.`department` (`dept_code`, `dept_name`, `univ_code`) VALUES ('phy', 'physics', 'bc');
INSERT INTO `university`.`department` (`dept_code`, `dept_name`, `univ_code`) VALUES ('mgmt', 'management', 'cd');
INSERT INTO `university`.`department` (`dept_code`, `dept_name`, `univ_code`) VALUES ('acc', 'accountancy', 'cd');

INSERT INTO `university`.`college_department` (`cdept_id`, `udept_code`, `collegeid`) VALUES ('1', 'ece', '110');
INSERT INTO `university`.`college_department` (`cdept_id`, `udept_code`, `collegeid`) VALUES ('2', 'ece', '100');
INSERT INTO `university`.`college_department` (`cdept_id`, `udept_code`, `collegeid`) VALUES ('3', 'eee', '111');
INSERT INTO `university`.`college_department` (`cdept_id`, `udept_code`, `collegeid`) VALUES ('4', 'eee', '103');
INSERT INTO `university`.`college_department` (`cdept_id`, `udept_code`, `collegeid`) VALUES ('5', 'mech', '109');
INSERT INTO `university`.`college_department` (`cdept_id`, `udept_code`, `collegeid`) VALUES ('6', 'mech', '113');
INSERT INTO `university`.`college_department` (`cdept_id`, `udept_code`, `collegeid`) VALUES ('7', 'it', '112');
INSERT INTO `university`.`college_department` (`cdept_id`, `udept_code`, `collegeid`) VALUES ('8', 'it', '115');

INSERT INTO `university`.`designation` (`id`, `name`, `rank`) VALUES ('6', 'student', 'S');

INSERT INTO `university`.`employee` (`id`, `name`, `dob`, `email`, `phone`, `college_id`, `cdept_id`, `desig_id`) VALUES ('10', 'nisha', '2000-05-18', 'nishaganesh@gmail.com', '9999988888', '100', '1', '6');
INSERT INTO `university`.`employee` (`id`, `name`, `dob`, `email`, `phone`, `college_id`, `cdept_id`, `desig_id`) VALUES ('20', 'supriya', '2000-06-09', 'supriya@yahoo.coom', '8787878764', '100', '2', '5');
INSERT INTO `university`.`employee` (`id`, `name`, `dob`, `email`, `phone`, `college_id`, `cdept_id`, `desig_id`) VALUES ('30', 'sonakshi', '2000-01-19', 'sonakshi@gmail.com', '989865745', '111', '4', '6');
INSERT INTO `university`.`employee` (`id`, `name`, `dob`, `email`, `phone`, `college_id`, `cdept_id`, `desig_id`) VALUES ('40', 'surya', '1999-05-30', 'surya@gmail.com', '70123458960', '103', '6', '4');
INSERT INTO `university`.`employee` (`id`, `name`, `dob`, `email`, `phone`, `college_id`, `cdept_id`, `desig_id`) VALUES ('50', 'vasudha', '2000-06-04', 'vasudha@gmail.com', '8453467985', '102', '8', '3');
INSERT INTO `university`.`employee` (`id`, `name`, `dob`, `email`, `phone`, `college_id`, `cdept_id`, `desig_id`) VALUES ('60', 'karthika', '2001-08-08', 'karthika@rediff.com', '8457964000', '100', '8', '4');
INSERT INTO `university`.`employee` (`id`, `name`, `dob`, `email`, `phone`, `college_id`, `cdept_id`, `desig_id`) VALUES ('70', 'deepa', '1989-10-19', 'deepa@yahoo.com', '703268951', '102', '1', '2');
INSERT INTO `university`.`employee` (`id`, `name`, `dob`, `email`, `phone`, `college_id`, `cdept_id`, `desig_id`) VALUES ('80', 'sanjeev', '1999-08-12', 'sanjeev@gmail.com', '8438400578', '103', '8', '1');
INSERT INTO `university`.`employee` (`id`, `name`, `dob`, `email`, `phone`, `college_id`, `cdept_id`, `desig_id`) VALUES ('90', 'nithi', '2000-12-15', 'nithi@gmail.com', '9500989386', '100', '2', '2');
INSERT INTO `university`.`employee` (`id`, `name`, `dob`, `email`, `phone`, `college_id`, `cdept_id`, `desig_id`) VALUES ('11', 'geetha', '1999-08-07', 'geetha@yahoo.com', '9965587522', '101', '1', '6');

INSERT INTO `university`.`syllabus` (`id`, `cdeptid`, `syllabus_code`, `syllabus_name`) VALUES ('90', '1', 'eces', 'electronic circuit');
INSERT INTO `university`.`syllabus` (`id`, `cdeptid`, `syllabus_code`, `syllabus_name`) VALUES ('91', '3', 'eees', 'electrical circuit');
INSERT INTO `university`.`syllabus` (`id`, `cdeptid`, `syllabus_code`, `syllabus_name`) VALUES ('92', '5', 'mecs', 'hydro mechanics');
INSERT INTO `university`.`syllabus` (`id`, `cdeptid`, `syllabus_code`, `syllabus_name`) VALUES ('93', '7', 'its', 'computer technology');
INSERT INTO `university`.`syllabus` (`id`, `cdeptid`, `syllabus_code`, `syllabus_name`) VALUES ('94', '2', 'ecs', 'communication network');

INSERT INTO `university`.`professor_syllabus` (`emp_id`, `syllabus_id`, `semester`) VALUES ('10', '91', '1');
INSERT INTO `university`.`professor_syllabus` (`emp_id`, `syllabus_id`, `semester`) VALUES ('11', '90', '7');
INSERT INTO `university`.`professor_syllabus` (`emp_id`, `syllabus_id`, `semester`) VALUES ('20', '92', '2');
INSERT INTO `university`.`professor_syllabus` (`emp_id`, `syllabus_id`, `semester`) VALUES ('40', '94', '6');
INSERT INTO `university`.`professor_syllabus` (`emp_id`, `syllabus_id`, `semester`) VALUES ('50', '93', '3');

INSERT INTO `university`.`student` (`id`, `roll_number`, `name`, `dob`, `gender`, `email`, `phone`, `address`, `academic_year`, `cdep_id`, `coll_id`) VALUES ('1001', 'one', 'Khushi', '2000-07-06', 'f', 'khushi@yahoo.com', '8754213690', 'periyar street,coimbatore', 2020, '8', '100');
INSERT INTO `university`.`student` (`id`, `roll_number`, `name`, `dob`, `gender`, `email`, `phone`, `address`, `academic_year`, `cdep_id`, `coll_id`) VALUES ('1002', 'two', 'vicas', '1999-05-13', 'm', 'vicas@gmail.com', '7635428910', 'laila colony,mysore', 2020, '7', '101');
INSERT INTO `university`.`student` (`id`, `roll_number`, `name`, `dob`, `gender`, `email`, `phone`, `address`, `academic_year`, `cdep_id`, `coll_id`) VALUES ('1003', 'three', 'shyam', '2000-08-17', 'm', 'shyam@hotmail.com', '8659647563', 'law street,karur', 2020, '6', '102');
INSERT INTO `university`.`student` (`id`, `roll_number`, `name`, `dob`, `gender`, `email`, `phone`, `address`, `academic_year`, `cdep_id`, `coll_id`) VALUES ('1004', 'four', 'jeeva', '1997-10-15', 'm', 'jeeva@hotmail.com', '896375421', 'ace bungalow,kanyakumari', 2022, '5', '103');
INSERT INTO `university`.`student` (`id`, `roll_number`, `name`, `dob`, `gender`, `email`, `phone`, `address`, `academic_year`, `cdep_id`, `coll_id`) VALUES ('1005', 'five', 'keerthi', '1999-09-18', 'f', 'keerthi@rediffmail.com', '7897897896', 'hb colony,hosur', 2021, '4', '104');
INSERT INTO `university`.`student` (`id`, `roll_number`, `name`, `dob`, `gender`, `email`, `phone`, `address`, `academic_year`, `cdep_id`, `coll_id`) VALUES ('1006', 'six', 'iniya', '1999-05-09', 'f', 'iniya@yahoo.com', '768321403', 'peelamedu,coimbatore', 2021, '3', '101');
INSERT INTO `university`.`student` (`id`, `roll_number`, `name`, `dob`, `gender`, `email`, `phone`, `address`, `academic_year`, `cdep_id`, `coll_id`) VALUES ('1007', 'seven', 'nivin', '1998-10-10', 'm', 'nivin@gmail.com', '65867596', 'lakshmi street,theni', 2021, '2', '102');
INSERT INTO `university`.`student` (`id`, `roll_number`, `name`, `dob`, `gender`, `email`, `phone`, `address`, `academic_year`, `cdep_id`, `coll_id`) VALUES ('1008', 'eight', 'vishwa', '2000-05-13', 'm', 'vishwa@yahoo.com', '968676564', 'gv residency,coimbatore', 2022, '1', '103');
INSERT INTO `university`.`student` (`id`, `roll_number`, `name`, `dob`, `gender`, `email`, `phone`, `address`, `academic_year`, `cdep_id`, `coll_id`) VALUES ('1009', 'nine', 'akash', '2000-07-12', 'm', 'akash@gmail.com', '668685942', 'nanjundapuram,chennai', 2019, '1', '111');
INSERT INTO `university`.`student` (`id`, `roll_number`, `name`, `dob`, `gender`, `email`, `phone`, `address`, `academic_year`, `cdep_id`, `coll_id`) VALUES ('1010', 'ten', 'anuja', '2000-07-12', 'f', 'anuja@gmail.com', '777777997', 'nanjundapuram,coimbatore', 2019, '2', '104');
INSERT INTO `university`.`student` (`id`, `roll_number`, `name`, `dob`, `gender`, `email`, `phone`, `address`, `academic_year`, `cdep_id`, `coll_id`) VALUES ('1011', 'eleven', 'arya', '2001-01-14', 'm', 'arya@gmail.com', '8888888887', 'parsn sesh nestle,coimbatore', 2018, '3', '100');
INSERT INTO `university`.`student` (`id`, `roll_number`, `name`, `dob`, `gender`, `email`, `phone`, `address`, `academic_year`, `cdep_id`, `coll_id`) VALUES ('1012', 'twelve', 'chamrutha', '2000-08-25', 'f', 'chamrutha@yahoo.com', '97989695', 'lakshmi gardens,vellore', 2020, '4', '102');
INSERT INTO `university`.`student` (`id`, `roll_number`, `name`, `dob`, `gender`, `email`, `phone`, `address`, `academic_year`, `cdep_id`, `coll_id`) VALUES ('1013', 'thirteen', 'farin', '2000-07-01', 'F', 'farin@gmail.com', '750012125', 'singanallur,coimbatore', 2021, '5', '103');
INSERT INTO `university`.`student` (`id`, `roll_number`, `name`, `dob`, `gender`, `email`, `phone`, `address`, `academic_year`, `cdep_id`, `coll_id`) VALUES ('1014', 'forteen', 'gokul', '2000-08-08', 'M', 'gokul@gmail.com', '8754963210', 'gandhinagar,trichy', 2022, '6', '104');
INSERT INTO `university`.`student` (`id`, `roll_number`, `name`, `dob`, `gender`, `email`, `phone`, `address`, `academic_year`, `cdep_id`, `coll_id`) VALUES ('1015', 'fifteen', 'harini', '1999-05-15', 'F', 'harinis@gmail.com', '657894212', 'indira nagar,chennai', 2022, '7', '105');
INSERT INTO `university`.`student` (`id`, `roll_number`, `name`, `dob`, `gender`, `email`, `phone`, `address`, `academic_year`, `cdep_id`, `coll_id`) VALUES ('1016', 'sixteen', 'Manish', '2000-02-23', 'M', 'manish@yahoo.com', '8978654310', 'ten down street,ariyalur', 2023, '8', '106');
INSERT INTO `university`.`student` (`id`, `roll_number`, `name`, `dob`, `gender`, `email`, `phone`, `address`, `academic_year`, `cdep_id`, `coll_id`) VALUES ('1017', 'seventen', 'Yadhav', '1998-05-26', 'M', 'yadhav@rediffmail.com', '65428971430', 'yellow colony,dharmapuri', 2018, '8', '109');
INSERT INTO `university`.`student` (`id`, `roll_number`, `name`, `dob`, `gender`, `email`, `phone`, `address`, `academic_year`, `cdep_id`, `coll_id`) VALUES ('1018', 'eighteen', 'regina', '1997-01-03', 'F', 'regina@yahoo.com', '6342859170', 'dubai main road,thirupur', 2019, '7', '100');
INSERT INTO `university`.`student` (`id`, `roll_number`, `name`, `dob`, `gender`, `email`, `phone`, `address`, `academic_year`, `cdep_id`, `coll_id`) VALUES ('1019', 'nineteen', 'nandhini', '2000-03-13', 'F', 'nandhini@gmail.com', '8976543210', 'bharathiyar nagar,chennai', 2018, '6', '101');
INSERT INTO `university`.`student` (`id`, `roll_number`, `name`, `dob`, `gender`, `email`, `phone`, `address`, `academic_year`, `cdep_id`, `coll_id`) VALUES ('1020', 'twenty', 'harsha', '1998-05-25', 'F', 'harsha@gmail.com', '638395210', '4th street,adayar,chennai', 2019, '5', '100');
INSERT INTO `university`.`student` (`id`, `roll_number`, `name`, `dob`, `gender`, `email`, `phone`, `address`, `academic_year`, `cdep_id`, `coll_id`) VALUES ('1021', 'twoone', 'malarvizhi', '2000-09-13', 'F', 'malar@gmail.com', '9786543213', 'murugan colony,madurai', 2019, '4', '104');
INSERT INTO `university`.`student` (`id`, `roll_number`, `name`, `dob`, `gender`, `email`, `phone`, `address`, `academic_year`, `cdep_id`, `coll_id`) VALUES ('1022', 'twotwo', 'jennifer', '2000-12-01', 'F', 'jenifer@gmail.com', '45781264', '8A,PGP appartment,coimbatore', 2018, '3', '104');
INSERT INTO `university`.`student` (`id`, `roll_number`, `name`, `dob`, `gender`, `email`, `phone`, `address`, `academic_year`, `cdep_id`, `coll_id`) VALUES ('1023', 'twothree', 'lokesh', '1999-06-19', 'M', 'lokesh@yahoo.com', '767685423252', '5th cross street,chennai', 2020, '2', '105');
INSERT INTO `university`.`student` (`id`, `roll_number`, `name`, `dob`, `gender`, `email`, `phone`, `address`, `academic_year`, `cdep_id`, `coll_id`) VALUES ('1024', 'twofour', 'pradeep', '2000-06-13', 'M', 'pradeep@gmail.com', '9595968632', '4th ganapathy street,coimbatore', 2022, '1', '102');





