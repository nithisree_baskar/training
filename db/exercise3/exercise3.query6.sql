6. Create new entries in SEMESTER_FEE table for each student from all the colleges and across all the universities. These entries should be
created whenever new semester starts.. Each entry should have below default values;
a) AMOUNT - Semester fees
b) PAID_YEAR - Null
c) PAID_STATUS - Unpaid
 
 
UPDATE `university`.`semester_fee` SET `amount` = '50000.00' WHERE (`amount` = '30000.00');
UPDATE `university`.`semester_fee` SET `amount` = '50000.00' WHERE (`amount` = '35000.00');
UPDATE `university`.`semester_fee` SET `amount` = '50000.00' WHERE (`amount` = '40000.00');
UPDATE `university`.`semester_fee` SET `amount` = '50000.00' WHERE (`amount` = '45000.00');
UPDATE `university`.`semester_fee` SET `amount` = '50000.00' WHERE (`amount` = '50500.00');

ALTER TABLE `university`.`semester_fee` 
CHANGE COLUMN `paid_status` `paid_status` VARCHAR(10) NULL DEFAULT 'unpaid' ;

ALTER TABLE `university`.`semester_fee` 
CHANGE COLUMN `amount` `amount` DOUBLE(18,2) NULL DEFAULT '45000.00' ;

 
 
 