9 
 
9. Show consolidated result for the following scenarios;
a) collected and uncollected semester fees amount per semester
for each college under an university. Result should be filtered
based on given year.
b) Collected semester fees amount for each university for the given
year 
a)

ALTER TABLE `university management`.`semester_fee` 
ADD COLUMN `balance_amount` DOUBLE NOT NULL AFTER `paid_status`;

SELECT university.`university_name`
      ,college.`name`
      ,semester_fee.`semester`
      ,SUM(amount) AS 'collected_fees' 
      ,semester_fee.paid_year
FROM semester_fee
    ,university
    ,student
    ,college
WHERE semester_fee.stud_id = student.id
 AND student.coll_id = college.id
 AND college.univ_code = university.univ_code
 AND university_name = 'vit'
 AND paid_year = '2020'
 AND semester = '7'
 AND paid_status = 'unpaid';

b) Collected semester fees amount for each university for the given 
    year  

SELECT university.`university_name`
      ,SUM(amount) AS 'collected_fees' 
      ,semester_fee.paid_year
FROM semester_fee
    ,university
    ,student
    ,college
WHERE semester_fee.stud_id = student.id
 AND student.coll_id = college.id
 AND college.univ_code = university.univ_code
 AND university.university_name = 'vit'
 AND paid_status = 'unpaid'
 AND paid_year = '2020'