5. List Students details along with their GRADE,CREDIT and GPA details from all universities. Result should be sorted by college_name and semester. Apply paging also.

SELECT student.roll_number
      ,student.name
      ,college.name
      ,semester_result.grade
      ,semester_result.credits
      ,semester_result.sem
      ,semester_result.gpa
FROM student 
      ,semester_result 
      ,college 
WHERE student.id=semester_result.st_id 
AND   college.id = student.coll_id
ORDER BY college.name,semester_result.sem
LIMIT 20
OFFSET 0;
