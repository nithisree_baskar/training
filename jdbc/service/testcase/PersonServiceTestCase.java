/*
Requirement:
    To test the conditions in the PersonService.
    
Entity:
    1.PersonServiceTestCase
    2.AddressService
    3.AppExcetion
    4.Person
    5.Address
    
Function declaration:
    public void setup() {}
    public void personCreationTest() {}
    public void personCreationTest1() {}
    public void personCreationTest2() throws AppException {}
    public void personCreationTest3() throws AppException {}
    public void personReadTest() {}
    public void personReadTest2() {}
    public void personReadTest1() {}
    public void personReadAllTest() {}
    public void personUpdationTest() {}
    public void personUpdationTest1() {}
    public void personUpdationTest2() {}
    public void personUpdationTest3() {}
    public void personDeleteTest() {}
    public void personDeleteTest1() {}
    
Jobs to be done:
    1. Create an instance of PersonService as personService in private.
        1.1 Declare address, address1,address2 of type Address in private.
        1.2 Declare person, person1, person2, person3, person4 of type Person in private.
        1.3 Declare id of type long in private.
    2. Inside a setup() method
        2.1 Set the respective values for the declared variables by creating objects of Person and Address.
    3. Priority one is to check the address creation of the person with postal_code as zero.
        3.1 Invoke the create method of personService
        3.2 Check whether it throws the expected AppException.
    4. Priority two is to check the address creation of the person with postal_code not as zero.
        4.1 Invoke the create method of personService and store the returned value in id.
        4.2 Check whether the id is greater than zero.
    5. Priority three is to check the address creation of the person with duplicate mailid.
        5.1 Invoke the create method of personService
        5.2 Check whether it throws the expected AppException.
    6. Priority four is to check the address creation of the person with unique mailid.
        6.1 Invoke the create method of personService and store the returned value in id.
        6.2 Check whether the id is greater than zero.
    7. Priority five  is to check the read method of the personService  with invalid id.
        7.1 Invoke the read method of personService.
        7.2 Check whether the returned value is equal to the expected value.
    8. Priority six is to check the read method of the personService with valid id and boolean flag is false.
        8.1 Invoke the read method of personService and store the returned values in person2.
        8.2 Check whether the returned values of name, email, birthdate, id is equal to the known value.
    9. Priority seven is to check the read method of the personService with valid id and boolean flag is true.
        9.1 Set the address values in person1.
        9.2 Invoke the read method of personService with Address and store the values in person2.
        9.2 Check whether the values of name, email, birthdate,id, street, city, postal_code, id is equal to the known person1 values.
    10. Priority eight is to check the readAll method of personService with address.
        10.1 Invoke the readAll method of personService.
        10.2 Check whether the returned values are not null.
    11. Priority nine is to check the updation of personService with postal_code not as zero and unique mailid.
        11.1 Invoke the update method with person3 and address2
        11.2 Set the values of address2 in person3.
        11.3 Invoke the read method and store the values in person2.
        11.4 Check whether the values of name, email, birthdate,id, street, city, postal_code, id is equal to the known person3 values.
    12. Priority ten is to check the updation of personService with postal_code as zero.
        12.1 Invoke the updation method of personService.
        12.2 Check whether it throws the expected AppException.
    13. Priority eleven is to check the updation of personService with duplicate mailid.
        13.1 Invoke the updation method of personService.
        13.2 Check whether it throws the expected AppException.
    14. Priority twelve is to check the updation of personService with invalid id.
        14.1 Invoke the updation method of personService.
        14.2 Check whether it throws the expected AppException.
    15. Priority thirteen is to check the deletion of personService with valid id.
        15.1 Invoke the delete method of personService.
        15.2 Invoke the read method of the deleted id.
        15.3 Check whether the returned value is equal to the null.
    16. Priority fourteen is to check the deletion of personService with invalid id.
        16.1 Invoke the delete method of personService.
        14.2 Check whether it throws the expected AppException.

Pseudo code:
    class PersonServiceTestCase {
    
        private PersonService personService = new PersonService();
        private Address address;
        private Address address1;
        private Address address2;
        private Person person;
        private Person person1;
        private Person person2;
        private Person person3;
        private Person person4;
        private long id;
    
    
        @BeforeClass
        public void setup() {
            person = new Person("Baddu", "bau@wgggqenirfrcww.com", Date.valueOf("2001-01-20"));
            person1 = new Person("Baddu", "adumegwicfnrqw.com", Date.valueOf("2001-01-20"));
            address = new Address("NGR Road", "Coimbatore", 0);
            address1 = new Address("NGR Road", "Tiruppur", 641605);
            person3 = new Person("Balaji", "@miawegfqrnwl.com", Date.valueOf("2020-10-30"));
            address2 = new Address("MG Road", "Bangalore", 628402);
            person4 = new Person("Balaji", "asbwf@ggfmmqwrail.com", Date.valueOf("2020-10-30"));
        }
    
        @Test(priority = 1, description = "Address Creation with postal_code as 0",
                expectedExceptions = {AppException.class},
                expectedExceptionsMessageRegExp = "ERR401 : postal code should not be zero")
        public void personCreationTest() {
            person.setAddress(address);
            personService.create(person);
        }
    
        @Test(priority = 2, description = "Address Creation with postal_code not as 0")
        public void personCreationTest1() {
            person.setAddress(address1);
            long id = personService.create(person);
            Assert.assertTrue(id > 0);
        }
    
        @Test(priority = 3, description = "Address creation with duplicate email",
                expectedExceptions = AppException.class,
                expectedExceptionsMessageRegExp = "ERR411 : Email should be unique")
        public void personCreationTest2() throws AppException {
            person.setAddress(address1);
            personService.create(person);
    
        }
    
        @Test(priority = 4, description = "Address creation with unique email")
        public void personCreationTest3() throws AppException {
            person1.setAddress(address1);
            this.id = personService.create(person1);
            Assert.assertTrue(id > 0);
        }
    
        @Test(priority = 5, description = "invalid id")
        public void personReadTest() {
            Assert.assertEquals(personService.read(0, true), null);
        }
    
        @Test(priority = 6, description = "Valid id and the boolean flag is false")
        public void personReadTest2() {
            person2 = personService.read(this.id, false);
            Assert.assertEquals(person2.getName(), person1.getName());
            Assert.assertEquals(person2.getEmail(), person1.getEmail());
            Assert.assertEquals(person2.getBirthDate(), person1.getBirthDate());
            Assert.assertEquals(person2.getId(), this.id);
        }
    
        @Test(priority = 7, description = "Valid id and the boolean flag is true")
        public void personReadTest1() {
            person1.setAddress(address1);
            person2 = personService.read(this.id, true);
            Assert.assertEquals(person2.getName(), person1.getName());
            Assert.assertEquals(person2.getEmail(), person1.getEmail());
            Assert.assertEquals(person2.getBirthDate(), person1.getBirthDate());
            Assert.assertEquals(person2.getId(), this.id);
            Assert.assertEquals(person2.getAddress().getStreet(), person1.getAddress().getStreet());
            Assert.assertEquals(person2.getAddress().getCity(), person1.getAddress().getCity());
            Assert.assertEquals(person2.getAddress().getPostalCode(),
                    person1.getAddress().getPostalCode());
    
        }
    
        @Test(priority = 8, description = "Reading all persons with address")
        public void personReadAllTest() {
    
            Assert.assertTrue(personService.readAll() != null);
        }
    
        @Test(priority = 9,
                description = "Address Updation with pincode not as 0 and email id is unique")
        public void personUpdationTest() {
            person3.setAddress(address2);
            personService.update(this.id, person3);
            person3.setAddress(address2);
            person2 = personService.read(this.id, true);
            Assert.assertEquals(person2.getName(), person3.getName());
            Assert.assertEquals(person2.getEmail(), person3.getEmail());
            Assert.assertEquals(person2.getBirthDate(), person3.getBirthDate());
            Assert.assertEquals(person2.getId(), this.id);
            Assert.assertEquals(person2.getAddress().getStreet(), person3.getAddress().getStreet());
            Assert.assertEquals(person2.getAddress().getCity(), person3.getAddress().getCity());
            Assert.assertEquals(person2.getAddress().getPostalCode(),
                    person3.getAddress().getPostalCode());
    
        }
    
        @Test(priority = 10, description = "Address Updation with pincode as 0",
                expectedExceptions = AppException.class,
                expectedExceptionsMessageRegExp = "ERR401 : postal code should not be zero")
        public void personUpdationTest1() {
            person3.setAddress(address);
            personService.update(this.id, person3);
        }
    
        @Test(priority = 11, description = "Address Updation with duplicate email",
                expectedExceptions = AppException.class,
                expectedExceptionsMessageRegExp = "ERR411 : Email should be unique")
        public void personUpdationTest2() {
            person.setAddress(address1);
            personService.update(this.id, person);
        }
    
        @Test(priority = 12, description = "Address Updation with invalid id",
                expectedExceptions = AppException.class,
                expectedExceptionsMessageRegExp = "ERR407 : Failed to update Person")
        public void personUpdationTest3() {
            person4.setAddress(address1);
            personService.update(100, person4);
        }
    
        @Test(priority = 13, description = "Deleting person with valid id")
        public void personDeleteTest() {
            personService.delete(this.id);
            Assert.assertEquals(personService.read(this.id, true), null);
        }
    
        @Test(priority = 14, description = "Deleting address with invalid id",
                expectedExceptions = AppException.class,
                expectedExceptionsMessageRegExp = "ERR408 : Failed to delete Person")
        public void personDeleteTest1() {
            personService.delete(100);
        }
    }
*/

package com.kpr.training.jdbc.test_case;

import java.sql.Date;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.model.Person;
import com.kpr.training.jdbc.service.PersonService;

public class PersonServiceTestCase {

    private PersonService personService = new PersonService();
    private Address address;
    private Address address1;
    private Address address2;
    private Person person;
    private Person person1;
    private Person person2;
    private Person person3;
    private Person person4;
    private long id;


    @BeforeClass
    public void setup() {
        person = new Person("Baddu", "bau@wgggqenirfrcww.com", Date.valueOf("2001-01-20"));
        person1 = new Person("Baddu", "adumegwicfnrqw.com", Date.valueOf("2001-01-20"));
        address = new Address("NGR Road", "Coimbatore", 0);
        address1 = new Address("NGR Road", "Tiruppur", 641605);
        person3 = new Person("Balaji", "@miawegfqrnwl.com", Date.valueOf("2020-10-30"));
        address2 = new Address("MG Road", "Bangalore", 628402);
        person4 = new Person("Balaji", "asbwf@ggfmmqwrail.com", Date.valueOf("2020-10-30"));
    }

    @Test(priority = 1, description = "Address Creation with postal_code as 0",
            expectedExceptions = {AppException.class},
            expectedExceptionsMessageRegExp = "ERR401 : postal code should not be zero")
    public void personCreationTest() {
        person.setAddress(address);
        personService.create(person);
    }

    @Test(priority = 2, description = "Address Creation with postal_code not as 0")
    public void personCreationTest1() {
        person.setAddress(address1);
        long id = personService.create(person);
        Assert.assertTrue(id > 0);
    }

    @Test(priority = 3, description = "Address creation with duplicate email",
            expectedExceptions = AppException.class,
            expectedExceptionsMessageRegExp = "ERR411 : Email should be unique")
    public void personCreationTest2() throws AppException {
        person.setAddress(address1);
        personService.create(person);

    }

    @Test(priority = 4, description = "Address creation with unique email")
    public void personCreationTest3() throws AppException {
        person1.setAddress(address1);
        this.id = personService.create(person1);
        Assert.assertTrue(id > 0);
    }

    @Test(priority = 5, description = "invalid id")
    public void personReadTest() {
        Assert.assertEquals(personService.read(0, true), null);
    }

    @Test(priority = 6, description = "Valid id and the boolean flag is false")
    public void personReadTest2() {
        person2 = personService.read(this.id, false);
        Assert.assertEquals(person2.getName(), person1.getName());
        Assert.assertEquals(person2.getEmail(), person1.getEmail());
        Assert.assertEquals(person2.getBirthDate(), person1.getBirthDate());
        Assert.assertEquals(person2.getId(), this.id);
    }

    @Test(priority = 7, description = "Valid id and the boolean flag is true")
    public void personReadTest1() {
        person1.setAddress(address1);
        person2 = personService.read(this.id, true);
        Assert.assertEquals(person2.getName(), person1.getName());
        Assert.assertEquals(person2.getEmail(), person1.getEmail());
        Assert.assertEquals(person2.getBirthDate(), person1.getBirthDate());
        Assert.assertEquals(person2.getId(), this.id);
        Assert.assertEquals(person2.getAddress().getStreet(), person1.getAddress().getStreet());
        Assert.assertEquals(person2.getAddress().getCity(), person1.getAddress().getCity());
        Assert.assertEquals(person2.getAddress().getPostalCode(),
                person1.getAddress().getPostalCode());

    }

    @Test(priority = 8, description = "Reading all persons with address")
    public void personReadAllTest() {

        Assert.assertTrue(personService.readAll() != null);
    }

    @Test(priority = 9,
            description = "Address Updation with pincode not as 0 and email id is unique")
    public void personUpdationTest() {
        person3.setAddress(address2);
        personService.update(this.id, person3);
        person3.setAddress(address2);
        person2 = personService.read(this.id, true);
        Assert.assertEquals(person2.getName(), person3.getName());
        Assert.assertEquals(person2.getEmail(), person3.getEmail());
        Assert.assertEquals(person2.getBirthDate(), person3.getBirthDate());
        Assert.assertEquals(person2.getId(), this.id);
        Assert.assertEquals(person2.getAddress().getStreet(), person3.getAddress().getStreet());
        Assert.assertEquals(person2.getAddress().getCity(), person3.getAddress().getCity());
        Assert.assertEquals(person2.getAddress().getPostalCode(),
                person3.getAddress().getPostalCode());

    }

    @Test(priority = 10, description = "Address Updation with pincode as 0",
            expectedExceptions = AppException.class,
            expectedExceptionsMessageRegExp = "ERR401 : postal code should not be zero")
    public void personUpdationTest1() {
        person3.setAddress(address);
        personService.update(this.id, person3);
    }

    @Test(priority = 11, description = "Address Updation with duplicate email",
            expectedExceptions = AppException.class,
            expectedExceptionsMessageRegExp = "ERR411 : Email should be unique")
    public void personUpdationTest2() {
        person.setAddress(address1);
        personService.update(this.id, person);
    }

    @Test(priority = 12, description = "Address Updation with invalid id",
            expectedExceptions = AppException.class,
            expectedExceptionsMessageRegExp = "ERR407 : Failed to update Person")
    public void personUpdationTest3() {
        person4.setAddress(address1);
        personService.update(100, person4);
    }

    @Test(priority = 13, description = "Deleting person with valid id")
    public void personDeleteTest() {
        personService.delete(this.id);
        Assert.assertEquals(personService.read(this.id, true), null);
    }

    @Test(priority = 14, description = "Deleting address with invalid id",
            expectedExceptions = AppException.class,
            expectedExceptionsMessageRegExp = "ERR408 : Failed to delete Person")
    public void personDeleteTest1() {
        personService.delete(100);
    }
}
