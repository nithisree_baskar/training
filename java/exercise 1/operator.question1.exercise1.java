
Requirement:
   To Change the following program to use compound assignments:
       class ArithmeticDemo {

            public static void main (String[] args){

                 int result = 1 + 2; // result is now 3
                 System.out.println(result);

                 result = result - 1; // result is now 2
                 System.out.println(result);

                 result = result * 2; // result is now 4
                 System.out.println(result);

                 result = result / 2; // result is now 2
                 System.out.println(result);

                 result = result + 8; // result is now 10
                 result = result % 7; // result is now 3
                 System.out.println(result);
            }
       }
	
Entities:
   It requires class named ArithmeticDemo.

Function Declaration:
   No function is declared in this program.
 
Job to be done:
   1.Read the code.
   2.Analyze the code.
   3.replace the operator with compound assignments.
   4.Check for similarity with the above code.

Solution:
  class ArithmeticDemo {

    public static void main (String[] args){
        int result = 3;
        System.out.println(result);

        result -= 1; 
        System.out.println(result);

        result *= 2; 
        System.out.println(result);

        result /= 2; 
        System.out.println(result);

        result += 8; 
        result %= 7; 
        System.out.println(result);

    }
}

Compound assignment operator consists of binary and simple assignment operator.
They perform the operation of the binary operator on both operands and store the result of that operation into the left operand.