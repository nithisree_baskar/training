
Requirement:
   To print Fibonacci series using for loop

Entities:
   Class named FibonacciFor is used.

Function Declaration:
   No function is declared in this program.
   
Job to be done:
   1. Understand the logic of fibonacci series
   2. get the number from the user.
   3. use for loop and print the number1 and number2
   4. then add the two numbers into a variable called sum
   5. Then print the number
   6. then assign the value of number2 in number1 and number2 as sum
   7. repeat the loop until it satisfies the condition
   
Solution:

import java.util.Scanner;

public class FibonacciFor {
public static void main(String[] args) {
  Scanner sc=new Scanner(System.in);
  int n,i,sum,num1=0,num2=1;
  System.out.println("Enter the maximum number");
  n=sc.nextInt();
  for(i=1;i<=n;i++) {
  
  System.out.println(num1+" ");
  sum=num1+num2;
  num1=num2;
  num2=sum;
  }
 }
 } 
 
 