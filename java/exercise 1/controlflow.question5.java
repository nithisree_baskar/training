
Requirement:
   To write a test program using braces, { and }, to further clarify the code.
     if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
        else System.out.println("second string");
        System.out.println("third string");
		
Entities:
   No class is used in this program.

Function Declaration:
   No function is declared in this program.

Job to be done:
   1. Clarify the above code using braces
   
Solution:
    if (aNumber >= 0){
        if (aNumber == 0){
            System.out.println("first string");}
        else {
		    System.out.println("second string");}
    System.out.println("third string");}