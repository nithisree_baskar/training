abstract class Parent{
    public abstract void height();
}
public class Child extends Parent{

   public void height(){
	System.out.println("Tall");
   }
   public static void main(String args[]){
	Parent obj = new Child();
	obj.height();
   }
}