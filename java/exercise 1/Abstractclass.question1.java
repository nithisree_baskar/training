/*Requirement:
  To Demonstrate abstract classes using Shape class.
  
Entities:
  abstract class Shape is used in this program. 
  classes Rectangle,Circle1,TestAbstraction1 are used.
  
Function Declaration:
  draw function is declared in this program.
  
Job to be done:
  1. Create an abstract class Shape and declare a function named draw.
  2. Create a class Rectangle and inherit Shape
  3. Create a class Circle1 and inherit Shape
  4. Create a main class TestAbstraction1 and create an object for Shape and call draw. */
  
abstract class Shape{  
abstract void draw();  
}  

class Rectangle extends Shape{  
void draw()
{System.out.println("drawing rectangle");}  
}  
class Circle1 extends Shape{  
void draw()
{System.out.println("drawing circle");}  
}  
class TestAbstraction1{  
public static void main(String args[]){  
Shape s=new Circle1();
s.draw();  
}  
}  
