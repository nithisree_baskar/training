
Requirement:
  In the following program, explain why the value "6" is printed twice in a row:
       class PrePostDemo {
           public static void main(String[] args){
               int i = 3;
               i++;
               System.out.println(i);    // "4"
               ++i;
               System.out.println(i);    // "5"
               System.out.println(++i);  // "6"
               System.out.println(i++);  // "6"
               System.out.println(i);    // "7"
           }
       }

Entities:
  It requires a function called PrePostDemo.

Function Declaration:
  No function is declared in this program.

Job to be done:
   1.Read the code.
   2.Analyze the code.
   3.Write the solution.

Solution:
   In the above code, 6 is printed twice because of pre increment operator first increments the value and prints the value. 
   Next is the post increment operator which first prints the value and then increments it.