
Requirement:
   To print the Fibonacci series using Recursion

Entities:
   Class named Fibonacci is used.

Function Declaration:
  function named fibonacciRecursion is declared.

Job to be done:
   1. Understand the logic of fibonacci
   2. create a class named Fibonacci
   3. declare variables in static int type
   4. create a function called fibonacciRecursion
   5. inside the function use if loop
   6. add the two numbers and store in sum 
   7. then assign the value of number2 in number1 and number2 as sum
   8. display the sum
   9. inside the main, get the value of n from user
  10. display the values of number1 and number2
  11. call the fibonacciRecursion function
  
Solution:

import java.util.Scanner;
public class Fibonacci{
	static int n1=0,n2=1,sum,i;
	static void fibonacciRecursion(int n){
		if(n>0) {
			sum=n1+n2;
			n1=n2;
			n2=sum;
			System.out.println(" "+ sum);
			fibonacciRecursion(n-1);
		}
	}
public static void main(String[] args) {
	int n;
	Scanner sc=new Scanner(System.in);
	n=sc.nextInt();
	System.out.println(n1+" "+n2);
	fibonacciRecursion(n-2);
}
}