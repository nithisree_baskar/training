
Requirement:
  To compare the enum values using equal method and == operator

Entities:
 No entities used for this question.
 
Function Declaration:
  No function is declared.
  
Job to be done:
  1.Comparison using equal method
  2.Comparison using == operator
 
Solution:
   enum is a special data type to which the user can assign predefined constants to variable. 
   Generally, == is NOT a viable alternative to equals. When it is, however (such as with enum), there are two important differences to consider:

   == never throws NullPointerException
   
sample code:
      enum Color { BLACK, WHITE };

      Color nothing = null;
      if (nothing == Color.BLACK); // runs fine
      if (nothing.equals(Color.BLACK)); // throws NullPointerException
      == is subject to type compatibility check at compile time

      enum Color { BLACK, WHITE };
      enum Chiral { LEFT, RIGHT };

      if (Color.BLACK.equals(Chiral.LEFT)); // compiles fine
      if (Color.BLACK == Chiral.LEFT); // DOESN'T COMPILE!!! Incompatible types!

 Immutable classes that have proper control over their instances can guarantee that == is usable. 
enum is specifically mentioned to exemplify.
