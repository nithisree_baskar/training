
Requirement:
   To write a test program containing the previous code snippet and make aNumber 3. What is the output of the program?
   if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
        else System.out.println("second string");
        System.out.println("third string");

Entities:
   No class is used in this program.

Function Declaration:
   No function is declared in this program.
   
Job to be done:
   1.Make changes in the code.
   2.Analyze the code.
   3.print the output.

Solution:
  aNumber=3;
  if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
            else 
			    System.out.println("second string");
  System.out.println("third string");
  
output:
   second string
   third string


3 is greater than or equal to 0, so execution progresses to the second if statement. The second if statement's test fails because 3 is not equal to 0. 
Thus, the else clause executes (since it's attached to the second if statement). 
Thus, second string is displayed. The final println is completely outside of any if statement, so it always gets executed,and thus third string is always displayed.
	