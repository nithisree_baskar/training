class OuterClass 
{
    static int outer_x = 5;

    int outer_y = 10; 

    private static int outer_private = 15; 

    static class StaticNestedClass 

    { 

        void display() 

        { 

            System.out.println("outer_x = " + outer_x); 

            System.out.println("outer_private = " + outer_private); 

        } 

    } 
} 

  

public class StaticNestedClassDemo 
{ 

    public static void main(String[] args) 

    { 
        OuterClass.StaticNestedClass nestedObject = new OuterClass.StaticNestedClass(); 
        nestedObject.display(); 

           } 
}