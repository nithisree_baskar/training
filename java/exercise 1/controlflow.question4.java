
Requirement:
   To write a test program using only spaces and line breaks, reformat the code snippet to make the control flow easier to understand.
      if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
        else System.out.println("second string");
        System.out.println("third string");

Entities:
   No class is used

Function Declaration:
   No function is declared in this program.

Job to be done:
   1. To reformat the code using only spaces and line breaks
  
Solution:
    
	if (aNumber >= 0)
          if (aNumber == 0)
              System.out.println("first string");
          else 
		      System.out.println("second string");
    System.out.println("third string");