
Requirement:
   To print Fibonacci series using while loop

Entities:
   Class named FibonacciWhile is used

Function Declaration:
  No function is declared in this program.
  
Job to be done:
    1. Understand the fibonacci logic.
	2. get the number from the user
	3. initialize i equal to 1
	4. use while loop and print the first number
	5. add the number1 and number 2 and store in sum
	6. then assign the value of number2 in number1 and number2 as sum
	7. increment i value

Solution:

import java.util.Scanner;
public class FibonacciWhile{
	 public static void main(String[] args) {
		 Scanner sc=new Scanner(System.in);
		 int n,i,sum,num1=0,num2=1;
		 System.out.println("Enter the maximum number");
		 n=sc.nextInt();
		 i=1;
		 while(i<=n)
		 {
			 System.out.println(num1+ " ");
			 sum=num1+num2;
			 num1=num2;
			 num2=sum;
			 i++;
		 }
	 }
 } 