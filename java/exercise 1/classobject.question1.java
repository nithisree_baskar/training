
Requirement:
  To find the class and instance variable of the following code.
  Consider the following class:
    public class IdentifyMyParts {
        public static int x = 7;
        public int y = 3;
    }

Entities:
  It requires class named IdentifyMyParts
  
Function Declaration:
  No function is declared in this program.
  
Job to be done:
  1. Analysing the code given.
  2. Find the class variable
  3. Find the instance variable
  4. Answer the questions
  
Solution:
- What are the class variables?
  Variable that is declared with static modifier is called class variable. From the above code, x is the class variable.

- What are the instance variables?
  An instance variable is a variable defined inside a class. Here, y is the instance variable.