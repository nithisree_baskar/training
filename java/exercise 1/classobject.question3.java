Requirements:
To find What's wrong with the following program? And fix it.

    public class SomethingIsWrong {
        public static void main(String[] args) {
            Rectangle myRect;
            myRect.width = 40;
            myRect.height = 50;
            System.out.println("myRect's area is " + myRect.area());
        }
    }
	
Entities:
SomethingIsWrong(given)

Function Declaration:
No function is declared in this program.

Job to be done:
       1.To create an object myRect 
	   2.Initialise values to the object
	   3.Print the values
	   
Solution:
Object creation is incomplete. The above code will not be able to create an object for Rectangle. It will create a null value for myRect object. 

The correct code will be 

public class SomethingIsWrong {
        public static void main(String[] args) {
            Rectangle myRect=new Rectangle();
            myRect.width = 40;
            myRect.height = 50;
            System.out.println("myRect's area is " + myRect.area());
        }
    }