
Requirement:
   To find the output of the code when aNumber is 3
	if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
        else System.out.println("second string");
        System.out.println("third string");

Entities:
    No class is used

Function Declaration:
    No function is declared in this program

Job to be done:
    1. Read the code.
	2. Understand the code.
	3. find the output of the code.

Solution:
        second string
		third string
	
	