/*
1.Requirement :
    -  Program to demonstrate schedule method calls of Timer class 

2.Entity:
    - Helper
    - TimerSchedule

3.Method Declaration:
    - public static void main(String[] args)

4.Jobs to be done :
     1.Create new TimerSchedule class store as object.
     2.Invoke Timer class and Helper class using TimerTask class.
     3.Invoke Date class .
         3.1)Schdule task and date using scheduleAtFixedRate method.
         3.2)Run synchronized method object and object wait method to object.
               3.2.1)Cancel timer using cancel method.
               3.2.2)Print the timer purge method.
     4.Check the number value is equal to 4 .
         4.1)Synchronized TimerSchedule class object using synchronized method.
         4.2)Notify the TimerSchedule object using notify method.
           
Pseudo Code:

class Helper extends TimerTask { 
	public static int i = 0; 
	public void run() { 
		System.out.println("Timer ran " + ++i); 
		if(i == 4) 
		{ 
			synchronized(TimerSchedule.obj) 
			{ 
				TimerSchedule.obj.notify(); 
			} 
		} 
	} 
} 
public class TimerSchedule { 

    //Create new TimerSchedule class store as object
	protected static TimerSchedule obj; 
	public static void main(String[] args) throws InterruptedException { 
	    //Invoke Timer class and Helper class using TimerTask class.
		obj = new TimerSchedule();  
		Timer timer = new Timer(); 
		TimerTask task = new Helper(); 
		Date date = new Date(); 
		timer.scheduleAtFixedRate(task, date, 5000); 
		System.out.println("Timer running"); 
		synchronized(obj) 
		{  
			obj.wait(); 
			timer.cancel(); 
			System.out.println(timer.purge()); 
		} 
	} 
} 
 */

package com.java.training.core.dateandtime;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

class Helper extends TimerTask {
	public static int i = 0;

	public void run() {
		System.out.println("Timer ran " + ++i);
		if (i == 4) {
			synchronized (TimerSchedule.obj) {
				TimerSchedule.obj.notify();
			}
		}
	}

}
public class TimerSchedule {

	protected static TimerSchedule obj;
	public static void main(String[] args) throws InterruptedException  {
		obj = new TimerSchedule();

		// creating a new instance of timer class
		Timer timer = new Timer();
		TimerTask task = new Helper();

		// instance of date object for fixed-rate execution
		Date date = new Date();

		timer.scheduleAtFixedRate(task, date, 5000);

		System.out.println("Timer running");
		synchronized (obj) {
			// make the main thread wait
			obj.wait();

			// once timer has scheduled the task 4 times,
			// main thread resumes
			// and terminates the timer
			timer.cancel();

			// purge is used to remove all cancelled
			// tasks from the timer'stak queue
			System.out.println(timer.purge());
		}

	}

}
