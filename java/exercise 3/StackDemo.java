/*
Requirement:
To Reverse List Using Stack with minimum 7 elements in list.

Entities:
class named StackDemo is used

Function Declaration:
public static void main(String[] args)

Jobs to be done:
1.Create a stack fruits and push elements to it.
2.Create a stream for fruits
3.using pop() method pop an element
4.using search() method search for the element in stack
5.using size() method print the size of the stack
6.print the stack elements print the stack elements
7.Create a new stack and push the elements in fruits to stack and remove it from fruits.
8.Pop the element from stack to fruits.
 */



package com.java.training.core.stackandqueue;
import java.util.Stack;
import java.util.stream.Stream;

public class StackDemo {

	public static void main(String[] args) {
		Stack<String> fruits=new Stack<String>();
		Stream<String> stream=fruits.stream();
		fruits.push("Apple");
		fruits.push("Orange");
		fruits.push("Grapes");
		fruits.push("Banana");
		fruits.push("Watermelon");
		fruits.add("Kiwi");
		fruits.pop();
		int index=fruits.search("Grapes");
		int size=fruits.size();
		System.out.println(size);
		System.out.println(index);
		stream.forEach((element) ->{
			System.out.println(element);
		});
		
		Stack<String> stack=new Stack<String>();
		while(fruits.size()>0) {
			stack.push(fruits.remove(0));
		}
		while(stack.size()>0) {
			fruits.add(stack.pop());
		}
		System.out.println(fruits);

	}

}
