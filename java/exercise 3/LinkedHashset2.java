/*
 Requirement:
 To demonstrate linked hash set to array() method in java

Entities:
A class named LinkedHashset2 is used

Function Declaration:
public static void main(String[] args) 

Jobs to be done:
1.Create a new LinkedHashSet named set
2.add elements to it and print set.
3.Create an Object array
4.convert the linked hash set to array and print the array
 */



package com.java.training.core.setsandmaps;
import java.util.*;
public class LinkedHashset2 {

	public static void main(String[] args) {
		LinkedHashSet<String> set=new LinkedHashSet<String>();
		set.add("Tomato");
		set.add("Onion");
		set.add("Potato");
		set.add("Brinjal");
		System.out.println(" The Linked Hashset : " +set);
		Object[] arr=set.toArray();
		System.out.println("The Array is :");
		for(int i=0;i<arr.length;i++) {
			System.out.println(arr[i]);
		}

	}

}
