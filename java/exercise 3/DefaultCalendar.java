/*
1.Requirement :
    - Program to get the dates 10 days before and after today. 
2.Entity:
    - TenDaysAfterBefore

3.Method declaration:
    - public static void main(String[] args)
    
4.Jobs To be Done:
    1.Invoke Calendar class getInstance method and store it in calender.
    2.Print year, month, date, hour, minute using Calendar class get method.

Pseudo code:
''''''''''''
public class DefaultCalender {
 public static void main(String[] args)
    {
        // Create a default calendar
        Calendar calendar = Calendar.getInstance();
        // Get and display information of current date from the calendar:
        System.out.println(calendar.get(Calendar.YEAR));
        System.out.println(calendar.get(Calendar.MONTH));
        System.out.println(calendar.get(Calendar.DATE));
        System.out.println(calendar.get(Calendar.HOUR));
        System.out.println(calendar.get(Calendar.MINUTE));
    }
}

 */

package com.java.training.core.dateandtime;
import java.util.Calendar;
public class DefaultCalendar {

	public static void main(String[] args) {
		
        Calendar calendar = Calendar.getInstance();
        System.out.println("Year: " + calendar.get(Calendar.YEAR));
        System.out.println("Month: " + calendar.get(Calendar.MONTH));
        System.out.println("Day: " + calendar.get(Calendar.DATE));
        System.out.println("Hour: " + calendar.get(Calendar.HOUR));
        System.out.println("Minute: " + calendar.get(Calendar.MINUTE));
	    System.out.println();

	}

}
