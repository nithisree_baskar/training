/*
Requirement:
To Explain about contains(), subList(), retainAll() and give example.

Entities:
Class named ListMethodDemo is used.

Function Declaration:
public static void main(String[] args)

Jobs to be done:
1.Create a package
2.Create a class named ListMethodDemo.
3.Create a List named list and add elements to it.
4.using contains() check if the element is used and print the result.
5.Create another list named oddlist and elements to it.
6.using retainAll() add the elements to list.
7.Create another list named string and all elements to it.
8.using subList() function add the elements to it.
9.Print the elements.
 */




package com.java.training.core.list;
import java.util.List;
import java.util.ArrayList;
public class ListMethodDemo {

	public static void main(String[] args) {
		List<Integer> list=new ArrayList<>();
		list.add(10);
		list.add(20);
		list.add(30);
		if(list.contains(25)==true) {
			System.out.println("Present");
		}
		else {
			System.out.println("Not Present");
		}
		List<Integer> oddList =new ArrayList<>();
		oddList.add(30);
		oddList.add(20);
		oddList.add(25);
		list.retainAll(oddList);
		System.out.println(" "+ list);
		List<String> string=new ArrayList<>();
		string.add("For");
		string.add("all");
		string.add("assessment");
		string.add("marks");
		string.add("uploaded");
		System.out.println(" " +string);
		List<String> substring=new ArrayList<>();
		substring=string.subList(2,4);
		System.out.println(" "+substring);

	}

}
