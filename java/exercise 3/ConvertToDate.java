/*
1.Requirement :
    - Program to convert a Calendar to Date 

2.Entity:
    - ConvertToDate

3.Method Declaration:
    - public static void main(String[] args)

4.Jobs to be done :
     1.Invoke Calendar class getInstance method and store in calendar.
     3.Using add method add one to Calender date.
          3.1)Get the add one date using Calender class getTime method and store it in date.
     4.Using SimpleDateFormat class set date and time that format.
          3.1)Set the date format using format method. 
     5.Print the date.
     
Pseudo Code:
public class ConvertToDate {
	public static void main(String[] args) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, 1);
		java.util.Date date = calendar.getTime();             
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String dateFormat = format.format(date); 
		System.out.println(dateFormat);
		java.util.Date inActiveDate = null;
		//using try block
		try {
		    inActiveDate = format.parse(dateFormat);
		} 
		//using catch block
		catch (ParseException e1) {
		    e1.printStackTrace();
		}
	}
}
 */

package com.java.training.core.dateandtime;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
public class ConvertToDate {

	public static void main(String[] args) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, 1);
		java.util.Date date = calendar.getTime();             
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String dateFormat = format.format(date); 
		System.out.println(dateFormat);
		@SuppressWarnings("unused")
		java.util.Date inActiveDate = null;
		try {
		    inActiveDate = format.parse(dateFormat);
		} catch (ParseException e1) {
		    
		    e1.printStackTrace();
		}

	}

}
