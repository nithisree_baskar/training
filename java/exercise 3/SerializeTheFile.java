/*
Create a File, get the Following fields as input from the user 
1.Name
2.studentId
3.Address
4.Phone Number
now store the input in the File and serialize it, and again deserialize the File and print the content.

Requirement:
  * Name
  * studentId
  * Address
  * Phone Number

Entity:
  * SerializeTheFile 

Method Signature:
  -none-

Jobs to be done:
  * Create a Class name SerializeTheFile and declare main method
  * Create a Object for Scanner name sc
  * Get input for name,stuentid,address,phonenumber and store it in  name,stuentid,address,phonenumber.
  * Use try
      => Set the path to store ser file and store it in fileout
      => Create a object for OutputStream name out for fileOut
      => Write s in out
  * Catch IOException
      => call method printStackTrace()

Psudocode:

public class SerializeTheFile {
	   public static void main(String [] args) {
		      Scanner sc = new Scanner(System.in);
		      Student s = new Student();
		      PRINT "Name: ";
		      s.name = sc.next();
		      PRINT "Student Id: ";
		      s.studentId = sc.next();
		      PRINT "Address: ";
		      s.address = sc.next();
		      PRINT "Phone Number: ";
		      s.phonenumber = sc.nextLong();
		      TRY {
		         FileOutputStream fileOut = new FileOutputStream("C:\\Users\\nithisree b\\eclipse-workspace\\javaee-demo\\src\\com\\java\\training\\core\\serialization");
		         ObjectOutputStream out = new ObjectOutputStream(fileOut);
		         out.writeObject(s);
		         out.close();
		         fileOut.close();
		         PRINT "Serialized data is saved in student.ser";
		      } CATCH IOException i {
		         i.printStackTrace();
		      }
		   }
}
 */

package com.java.training.core.serialization;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Scanner;
public class SerializeTheFile {

	public static void main(String[] args) {
		  Scanner sc = new Scanner(System.in);
	      Student s = new Student();
	      System.out.println("Name: ");
	      s.name = sc.next();
	      System.out.println("Student Id: ");
	      s.studentId = sc.next();
	      System.out.println("Address: ");
	      s.address = sc.next();
	      System.out.println("Phone Number: ");
	      s.phonenumber = sc.nextLong();
	      try {
	         FileOutputStream fileOut = new FileOutputStream("C:\\Users\\nithisree b\\eclipse-workspace\\javaee-demo\\src\\com\\java\\training\\core\\serialization");
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         out.writeObject(s);
	         out.close();
	         fileOut.close();
	         System.out.println("Serialized data is saved in Student.ser");
	      } catch (IOException i) {
	         i.printStackTrace();
	      }

	}

}
