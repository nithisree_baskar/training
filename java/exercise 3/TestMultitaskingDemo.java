/*
Requirement:
To Write a program of performing two tasks by two threads that implements Runnable interface.

Entities:
class TestMultitaskingDemo is used

Function Declaration:
public static void main(String[] args)
public void run()

Jobs to be done:
1.Create a package
2.Create a runnable interface r1 which has function run in it.
3.Create a Runnable interface r2 which has function run in it.
4.Create a new Thread t1 and t2 with r1 and r2 respectively.
5.Using start() method print the result.
 */







package com.java.training.core.concurrency;

public class TestMultitaskingDemo {

	public static void main(String[] args) {
		Runnable r1=new Runnable() {
			public void run() {
				System.out.println("task one");
			}
		};
		Runnable r2=new Runnable() {
			public void run() {
				System.out.println("task two");
			}
		};
		Thread t1=new Thread(r1);
		Thread t2=new Thread(r2);
		t1.start();
		t2.start();

	}

}
