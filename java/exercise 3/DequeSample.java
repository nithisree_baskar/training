/*
Requirement:
To use addFirst(),addLast(),removeFirst(),removeLast(),peekFirst(),peekLast(),pollFirst(),poolLast() methods to store and retrieve elements in ArrayDequeue

Entities:
A class named DequeSample is used

Function Declaration:
public static void main(String[] args)

Jobs to be done:
1.Create a class DequeSample
2.Create an ArrayDeque and add elements.
3.Use addFirst(),addLast(),removeLast(),removeFirst(),pollFirst(),pollLast(),peekFirst(),peekLast() functions and print the result.
 */



package com.java.training.core.collections;
import java.util.Deque;
import java.util.ArrayDeque;
public class DequeSample {

	public static void main(String[] args) {
		Deque<String> deque=new ArrayDeque<>();
		deque.add("Apple");
		deque.addFirst("Orange");
		deque.add("Grapes");
		deque.add("Mango");
		deque.add("Banana");
		deque.add("Lemon");
		deque.addLast("Strawberry");
		System.out.println(" "+deque);
		String r=deque.removeLast();
		System.out.println(" "+r);
		System.out.println(" "+deque);
		String t=deque.removeFirst();
		System.out.println(" "+t);
		System.out.println(" "+deque);
		String poll=deque.pollFirst();
		System.out.println(" "+poll);
		System.out.println(" "+deque);
		String x=deque.pollLast();
		System.out.println(" "+x);
		System.out.println(" "+deque);
        String f=deque.peekFirst();
        System.out.println(" "+f);
        String l=deque.peekLast();
        System.out.println(" "+l);
        
	}

}
