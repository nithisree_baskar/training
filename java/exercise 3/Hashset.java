/*
Requirement:
To demonstrate adding elements, displaying, removing, and iterating in hash set
 
Entities:
class named Hashset is used

Function Declaration:
public static void main(String[] args)

Jobs to be done:
1.Create a new HashSet named set
2.add elements to it
3.Using iterator method print the elements in set
4.Using remove() method remove a value and print the remaining set
 */




package com.java.training.core.setsandmaps;
import java.util.*;
public class Hashset {

	public static void main(String[] args) {
		HashSet<String> set=new HashSet<String>();
		set.add("One");
		set.add("Two");
		set.add("Three");
		set.add("Four");
		set.add("Five");
		set.add("Six");
		set.add("Five");
		Iterator<String> i=set.iterator();
		while(i.hasNext()) {
			System.out.println(i.next());
		}
		set.remove("Five");
		System.out.println(" "+set);
		
		
	}

}
