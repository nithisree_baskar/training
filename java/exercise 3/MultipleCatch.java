/*
Requirement:
To Demonstrate the catching multiple exception with example. 

Entities:
Class named MultipleCatch is used.

Function Declaration:
public static void main(String[] args)

Jobs to be done:
1.Create a package
2.Create a class MultipleCatch
3.Create an array and insert elements
4.Use multiple catch block and print the output.



*/
package com.java.training.core.exception;

public class MultipleCatch {

	public static void main(String[] args) {
		int[] arr=new int[] {1,2,3,4,5};
		for(int i=0;i<5;i++) {
			try {
				System.out.println(2/arr[i]);
			}
			catch(ArrayIndexOutOfBoundsException e){
				System.out.println("Array index out of range");
			}
			catch(Exception e) {
				System.out.println("Exception is handled");
			}
		}

	}

}
