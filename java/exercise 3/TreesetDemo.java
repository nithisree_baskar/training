/*
Requirement:
To demonstrate insertions and string buffer in tree set
 
Entities:
public class TreesetDemo implements Comparator<StringBuffer>

Function Declaration:
public static void main(String[] args)

Jobs to be done:
1.Create a new Tree set named tset
2.Add elements to it
3.print tset
4.Create another Tree set named tset1
5.add the same elements to it using StringBuffer() method.
6.Print the tree set tset1.

 */



package com.java.training.core.setsandmaps;
import java.util.Comparator;
import java.util.TreeSet;

public class TreesetDemo implements Comparator<StringBuffer>{
 @Override 
 public int compare(StringBuffer strB1, StringBuffer strB2) {
	 return strB1.toString().compareTo(strB2.toString());
 }
	public static void main(String[] args) {
	  TreeSet<String> tset=new TreeSet<>();
	  tset.add("One");
	  tset.add("Two");
	  tset.add("Three");
	  tset.add("Four");
	  tset.add("Five");
	  System.out.println(tset);
	  TreeSet<StringBuffer> tset1=new TreeSet<>(new TreesetDemo());
	  tset1.add(new StringBuffer("One"));
	  tset1.add(new StringBuffer("Two"));
	  tset1.add(new StringBuffer("Three"));
	  tset1.add(new StringBuffer("Four"));
	  tset1.add(new StringBuffer("Five"));
	  System.out.println(tset1);

	}

}
