/*
Requirements:
    -  sort the roster list based on the person's age in descending order using comparator
    
Entities:
    - AgeSorter
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create a class AgeSorter
    - Declare and define main function
    - Invoke createRoster() method of class-Person and assign the returned value to a list-persons
    - Use stream method and its sorted method to sort them in descending order and store it in a list-orderedPersons
    - Print the elements of orderedPersons
    
Psuedocode:
 
 public class AgeSorter {

	public static void main(String[] args) {
	   
	   //Create a list
		List<Person> persons = Person.createRoster();
		List<Person> orderedPersons = persons.stream()
				                             .sorted(Comparator.comparing(Person::getAge).reversed())
				                             .collect(Collectors.toList());
	    //using for loop
		for(Person person : orderedPersons) {
			System.out.println(person.getName() + " - " + person.getAge());
		} 
 */

package com.java.training.core.StreamsExercise;
import java.util.Comparator;
import java.util.List;
import java.util.stream.*;
public class AgeSorter {

	public static void main(String[] args) {
		List<Person> persons = Person.createRoster();
		List<Person> orderedPersons = persons.stream()
				                             .sorted(Comparator.comparing(Person::getAge).reversed())
				                             .collect(Collectors.toList());
		for(Person person : orderedPersons) {
			System.out.println(person.getName() + " - " + person.getAge());
		}
	}

}
