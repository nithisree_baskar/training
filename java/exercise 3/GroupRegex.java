/*
Requirement:
    To demonstrate the different types of groups in regex.

Entities:
    GroupRegex is the class

Function Declaration:
    public static void main(String[] args)
    
Jobs to be done:
1.create a class
2.get the regex to be checked
3.Create a pattern from regex
4.Get the String to be matched
5.create a matcher for the input String
6.Get the current matcher state
7.Using while loop get the group matched using group() method.

Pseudocode:

public class GroupRegex {

	public static void main(String[] args) {
		
		  // Get the regex to be checked 
        String regex = "(people)"; 
  
        // Create a pattern from regex 
        Pattern pattern = Pattern.compile(regex); 
  
        // Get the String to be matched 
        String stringToBeMatched = "Hey peeps"; 
  
        // Create a matcher for the input String 
        Matcher matcher = pattern .matcher(stringToBeMatched); 
  
        // Get the current matcher state 
        MatchResult result = matcher.toMatchResult(); 
        System.out.println("Current Matcher: "+ result); 
  
        while (matcher.find()) { 
            // Get the group matched using group() method 
            System.out.println(matcher.group()); 
 */



package com.java.training.core.regex;
import java.util.regex.*;
public class GroupRegex {

	public static void main(String[] args) {
		
        String regex = "(people)"; 
        Pattern pattern = Pattern.compile(regex); 
        String stringToBeMatched = "Hey peeps"; 
        Matcher matcher = pattern .matcher(stringToBeMatched); 
        MatchResult result = matcher.toMatchResult(); 
        System.out.println("Current Matcher: "+ result); 
  
        while (matcher.find()) { 
            
            System.out.println(matcher.group()); 
        } 

	}

}
