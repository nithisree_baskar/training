/*
Requirement:
To demonstrate program explaining basic add and traversal operation of linked hash set
 
Entities:
class named LinkedHashSet1 is used

Function Declaration:
public static void main(String[] args)

Jobs to be done:
1.Create a package
2.Create a LinkedHashSet named list and add elements to it
3.Using iterator print the elements in list.
 */




package com.java.training.core.setsandmaps;
import java.util.*;
public class LinkedHashSet1 {

	public static void main(String[] args) {
		LinkedHashSet<String> list=new LinkedHashSet<String>();
		list.add("Apple");
		list.add("Orange");
		list.add("Mango");
		list.add("Grapes");
		list.add("Banana");
		Iterator<String> i=list.iterator();
		while(i.hasNext()) {
			System.out.println(i.next());
		}

	}

}
