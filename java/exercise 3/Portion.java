/*
Requirement:
   To Write a Java program to get the portion of a map whose keys range from a given key to another key
 
Entities:
Class named Portion is used.

Function Declaration:
No function is declared in this program.

Jobs to be done:
1.Create a package.
2.Create a class named Portion
3.Create an empty TreeMap and enter elements in it.
4.Print the map elements
5.Create a subtree and using subMap insert elements in it.
6.Print the output
 */




package com.java.training.core.Map;
import java.util.*;
import java.util.Map.Entry;
public class Portion {

	public static void main(String[] args) {
		TreeMap<Integer,String> treemap=new TreeMap<Integer,String>();
		SortedMap<Integer,String> subtreemap=new TreeMap<Integer,String>();
		treemap.put(1, "Apple");
		treemap.put(2, "Orange");
		treemap.put(3, "Grapes");
		treemap.put(4, "Kiwi");
		treemap.put(5, "Strawberry");
		System.out.println("Original Tree map:" +treemap);
		subtreemap=treemap.subMap(2,4);
		System.out.println("Sub map key values: " +subtreemap);

	}

}
