/*
 Requirement:
 To Write a Java program to copy all of the mappings from the specified map to another map.
 
 Entities:
 class named CreateMap is used.
 
 Function Declaration:
 No function is declared in this program.
 
 Jobs to be done:
 1.Create a package
 2.Import util HashMap and Map packages
 3.create a class named CreateMap.
 4.create a map m and put the elements into it.
 5.print the map elements.
 6.Now create another Map and another elements to it.
 7.Using putAll() function put the elements of map b into map m and print it.
 */


package com.java.training.core.Map;
import java.util.HashMap;
import java.util.Map;
public class CreateMap {

	public static void main(String[] args) {
		Map<Integer,String> m=new HashMap<Integer,String>();
		m.put(1, "C");
		m.put(2, "C++");
		m.put(3,"Java");
		System.out.println(" "+m);
		Map<Integer,String> a=new HashMap<Integer,String>();
		a.put(4,"Python");
		a.put(5,"Bash");
		System.out.println(" "+a);
		m.putAll(a);
		System.out.println(" "+m);

	}

}
