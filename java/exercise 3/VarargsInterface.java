/*
1.Requirements
   - Code a functional program that can return the sum of elements of varArgs, passed into the  method of functional interface.
2.Entities
   - VarArgsInterface
   - MultipleValues (Interface)
3.Function Declaration
   - int sum(int... numbers);
   - public static void main(String[] args)
4.Jobs to be done
   1.Create a class as VarArgsInterface with interface as MultipleValues.
   2.Inside the declaring the single method with multiple integer parameters using varargs.
   3.In the class main creating interface object and assigning with passing integer values returning the addition of two values.
   4.Print statement invoking the interface single method with multiple integer value and finally return all added value using assigned
interface object lambda expression.
5.Psuedocode:
    
    //create an interface
     interface MultipleValues {
	     int sum(int... numbers);
	     
	 public class VarargsInterface {

	     public static void main(String[] args) {
	     
	     //create an object 
	      MultipleValues multipleValues = (int... numbers) -> {
	        int sum = 0;
			for (int i : numbers)  
			    sum += i;  
			  return sum; 
			};
			
		//print the result invoking interface single method with multiple integer value
		 System.out.println(multipleValues.sum(2,5,6,3,8));

 */




package com.java.training.core.lambdaexpressions;

interface MultipleValues {
	int sum(int... numbers);
}
public class VarargsInterface {

	public static void main(String[] args) {
		MultipleValues multipleValues = (int... numbers) -> {
			int sum = 0;
			for (int i : numbers)  
			    sum += i;  
			  return sum; 
			};
			System.out.println(multipleValues.sum(2,5,6,3,8));
	}

}
