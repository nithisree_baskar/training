/*
Requirement:
To compare the checked and unchecked exception.

Entities:
Class named CheckedUnchecked is used.

Function Declaration:
public static void main(String[] args)
static void error()

Jobs to be done:
1.Create a package 
2.Create a function error()
3.using try catch block and print the result
4.initialize two variables a and b
5.Print the result of a divided by b.
 */



package com.java.training.core.exception;

public class CheckedUnchecked {
	static void error() throws Exception{
		throw new Exception();
	}
	public static void main(String[] args) {
		try {
			error();
		}
		catch(Exception e) {
			System.out.println("No such method found");
		}
		int a=5;
		int b=0;
		System.out.println(a/b);
		

	}

}
