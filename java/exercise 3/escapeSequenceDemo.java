/*
Requirement:
To write a program to display the following using escape sequence in java
      A.My favorite book is "Twilight" by Stephanie Meyer
      B.She walks in beauty, like the night, 
        Of cloudless climes and starry skies 
        And all that's best of dark and bright 
        Meet in her aspect and her eyes�
      C."Escaping characters", � 2019 Java
 
Entities:
escapeSequenceDemo class is used.

Function Declaration:
public static void main(String[] args)

Jobs to be done:
1. Create a class escapeSequenceDemo
2. Create a string myFavouriteBook and print the string using escape sequences
3. Create a string byron and print the byron string using escape sequences
4. Print the result 

Psuedocode:
public class escapeSequenceDemo {

	public static void main(String[] args) {
	//create a String 
	 String myFavoriteBook = new String ("My favorite book is \"Twilight\" by Stephanie Meyer");
	 System.out.println(myFavoriteBook);
	//create another String
     String byron = new String ("She walks in beauty, like the night, \nOf cloudless climes and starry skies\nAnd all that's best of dark and bright\nMeet in her aspect and her eyes...");
     System.out.println(byron);
     System.out.println("\"Escaping characters\", \u00A9 2019 Java");

 */




package com.java.training.core.logicaloperator;

public class escapeSequenceDemo {

	public static void main(String[] args) {
		String myFavoriteBook = new String ("My favorite book is \"Twilight\" by Stephanie Meyer");
	    System.out.println(myFavoriteBook);
	    String byron = new String ("She walks in beauty, like the night, \nOf cloudless climes and starry skies\nAnd all that's best of dark and bright\nMeet in her aspect and her eyes...");
	    System.out.println(byron);
	    
	   System.out.println("\"Escaping characters\", \u00A9 2019 Java");
	}

}
