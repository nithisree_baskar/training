/*
Requirement:
To Create a set
    1.To Add 10 values
    2.To Perform addAll() and removeAll() to the set.
    3.To Iterate the set using 
        - Iterator
        - for-each
Entities:
class named CreateSet is used.

Function Declared:
No function is declared in this program.

Jobs to be done:
   1.Create a package
   2.import util package
   3.Declare the class CreateSet.
   4.Create an empty set and add elements and print it
   5.Use addAll() function and add elements to the set
   6.Using Iterator and for loop print the elements.
*/
package com.java.training.core.set;
import java.util.*;

public class CreateSet {

	public static void main(String[] args) {
		Set<Integer> s=new HashSet<Integer>();
		s.add(100);
		s.add(200);
		s.add(300);
		s.add(400);
		s.add(500);
		s.add(600);
		s.add(700);
		s.add(800);
		s.add(900);
		s.add(1000);
		System.out.println("Set : "+ s);
		Set<Integer> a=new HashSet<Integer>();
		a.add(1100);
		a.add(1200);
		a.add(1300);
		s.addAll(a);
		System.out.println("Set is "+ s);
        s.removeAll(a);
        System.out.println(s);
        Iterator<Integer> iterator=s.iterator();
        while(iterator.hasNext()) {
        	Integer element=iterator.next();
        	System.out.println("The elements are: "+ element);
        	}
        for(Integer i : s) {
    	    System.out.println(i);}
       boolean isEmpty = (s.size() == 0);
       System.out.println(isEmpty);
	}

}
