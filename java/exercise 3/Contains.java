/*
Requirement:
   To Write a Java program to test if a map contains a mapping for the specified key
 
Entities:
Class named Contains is used.

Function Declaration:
No function is declared in this program.

Jobs to be done:
1.Create a package.
2.Create a class named Contains
3.Create an empty Map and enter elements in it.
4.Print the map elements
5.Using contains() function check if a key is present.
6.Print the output
 */



package com.java.training.core.Map;
import java.util.*;
public class Contains {

	public static void main(String[] args) {
		HashMap<String,Integer> hashmap=new HashMap<String,Integer>();
		hashmap.put("Apple", 1);
		hashmap.put("Orange", 2);
		hashmap.put("Grapes", 3);
		hashmap.put("Banana", 4);
		hashmap.put("Strawberry", 5);
		System.out.println("Original Map :" + hashmap);
		System.out.println("1. Is key 'Orange' exists?");
		if(hashmap.containsKey("Orange")) {
			System.out.println("yes " +hashmap.get("Orange"));
		}
		else {
			System.out.println("no");
		}
		

	}

}
