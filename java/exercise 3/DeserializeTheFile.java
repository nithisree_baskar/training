/*
Create a File, get the Following fields as input from the user 
1.Name
2.studentId
3.Address
4.Phone Number
now store the input in the File and serialize it, and again de serialize the File and print the 
content.

Requirement:
  * ser file

Entity:
  *DeserializeTheFile 

Method Signature:
  -none-

Jobs to be done:
  * Create a Class name DeserializeTheFile and declare main method
  * Create a Object for Student S it is equla to NULL.
  * Use try
      => From the path to store ser file and store it in filein
      => Create a object for OutputStream name in for filein
      => Read s in in
  * Catch IOException
      => call method printStackTrace()
  * Catch  ClassNotFoundExceptio
      => print class not found.
  * Print name ,studentId,Address,Phone Number.        

Psudocode:

public class DeserializeTheFile {
	   public static void main(String [] args) {
		      Student s = null;
		      TRY {
		         FileInputStream fileIn = new FileInputStream("C:\\\\Users\\\\nithisree b\\\\eclipse-workspace\\\\javaee-demo\\\\src\\\\com\\\\java\\\\training\\\\core\\\\serialization");
		         ObjectInputStream in = new ObjectInputStream(fileIn);
		         s = (Student) in.readObject();
		         in.close();
		         fileIn.close();
		      } CATCH IOException i {
		         i.printStackTrace();
		         return;
		      } CATCH ClassNotFoundException c {
		         PRINT "Student class not found";
		         c.printStackTrace();
		         return;
		      }
		      
		      PRINT "Deserialized Employee...";
		      PRINT"Name: " + s.name;
		      PRINT "StudentId: " + s.studentId;
		      PRINT "Address: " + s.address;
		      PRINT "Phone Number: " + s.phonenumber;

	   }
}	
*/


package com.java.training.core.serialization;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
public class DeserializeTheFile {

	public static void main(String[] args) {
		Student s = null;
	      try {
	         FileInputStream fileIn = new FileInputStream("C:\\\\Users\\\\nithisree b\\\\eclipse-workspace\\\\javaee-demo\\\\src\\\\com\\\\java\\\\training\\\\core\\\\serialization");
	         ObjectInputStream in = new ObjectInputStream(fileIn);
	         s = (Student) in.readObject();
	         in.close();
	         fileIn.close();
	      } catch (IOException i) {
	         i.printStackTrace();
	         return;
	      } catch (ClassNotFoundException c) {
	         System.out.println("Student class not found");
	         c.printStackTrace();
	         return;
	      }
	      
	      System.out.println("Deserialized Employee...");
	      System.out.println("Name: " + s.name);
	      System.out.println("StudentId: " + s.studentId);
	      System.out.println("Address: " + s.address);
	      System.out.println("Phone Number: " + s.phonenumber);
	}

}
