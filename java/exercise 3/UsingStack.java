/*
Requirement:
To Create a stack using generic type and implement
  -> Push atleast 5 elements
  -> Pop the peek element
  -> search a element in stack and print index value
  -> print the size of stack
  -> print the elements using Stream
  
Entities:
A class named UsingStack is used.

Function Declaration:
public static void main(String[] args) 

Jobs to be done:
1.Create a package
2.Create a new Stack z and add elements to it
3.print the stack elements
4.using peek() method print the popped element
5.using search() method check is a string is present in the stack
6.using size() method print the size of the stack
7.Using stream print the elements

 */




package com.java.training.core.stackandqueue;
import java.util.*;
import java.util.stream.Stream;

public class UsingStack {

	public static void main(String[] args) {
     Stack<String> z=new Stack<String>();
     z.push("this");
     z.push("is");
     z.push("the");
     z.push("sample");
     z.push("program");
     System.out.println(" "+z);
     
     z.push("Hello");
     z.push("world");
     System.out.println("Final Stack: "+z);
     String topElement=z.peek();
     System.out.println("Popped element: " +topElement);
     String str="is";
     System.out.println("Search element " +z.search(str));
     int size=z.size();
     System.out.println("size :"+size);
     Stream stream=z.stream();
     stream.forEach((element)-> {
    	 System.out.println(element);
     });
     
     

	}

}
