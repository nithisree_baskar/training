/*
Requirements:
    - Consider a following code snippet:
      List<Integer> randomNumbers = Array.asList({1, 6, 10, 25, 78})
        - Find the sum of all the numbers in the list using java.util.Stream API
        - Find the maximum of all the numbers in the list using java.util.Stream API
        - Find the minimum of all the numbers in the list using java.util.Stream API
 
Entities:
    - ArrayStreamDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class ArrayStreamDemo
    - Declare and define main function
    - Create a array with 5 integers
    - Add all values in the array by mapping them as integer with mapToInt() method and add them with sum() method
    - Print the added value
    - Find the maximum of values in the array with max() method and print the value
    - Find the minimum value in the array with min() method and print the value 

Psuedocode:
public class ArrayStreamDemo {

	public static void main(String[] args) {
	    
	    //create a list
		List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 25, 78);
		
		//finding the sum using map
    	System.out.println("Sum : " + randomNumbers.stream().mapToInt(value -> value).sum());
    	
    	//finding the maximum in the list
    	System.out.println("Maximum : " + randomNumbers.stream().max(Integer::compare).get());
    	
    	//finding the minimum in the list
    	System.out.println("Minimum : " + randomNumbers.stream().min(Integer::compare).get());
*/

package com.java.training.core.StreamsExercise;
import java.util.List;
import java.util.Arrays;
import java.util.stream.*;
public class ArrayStreamDemo {

	public static void main(String[] args) {
		List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 25, 78);
    	System.out.println("Sum : " + randomNumbers.stream().mapToInt(value -> value).sum());
    	System.out.println("Maximum : " + randomNumbers.stream().max(Integer::compare).get());
    	System.out.println("Minimum : " + randomNumbers.stream().min(Integer::compare).get());

	}

}
