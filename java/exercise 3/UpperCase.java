/*
Requirement:
To convert the given list to uppercase.

Entities:
A class named UpperCase is used

Function Declaration:
public static void main(String[] args)

Jobs to be done:
1.Create an Array list
2.Add elements to it
3.using stream().map function convert each list element to uppercase and print it.
 */



package com.java.training.core.collections;
import java.util.List;
import java.util.ArrayList;
public class UpperCase {

	public static void main(String[] args) {
		List<String> list=new ArrayList<String>();
		list.add("Madurai");
		list.add("Coimbatore");
		list.add("Theni");
		list.add("Chennai");
		list.add("karur");
		list.add("Salem");
		list.add("Erode");
		list.add("Trichy");
		list.stream().map(district-> district.toUpperCase()).forEach(district -> System.out.print(district + " "));

	}

}
