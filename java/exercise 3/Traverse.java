/*
Requirement:
To create an array list with 7 elements, and create an empty linked list add all elements of the array list to linked list ,traverse the elements and display the result

Entities:
Class named Traverse is used

Function Declaration:
public static void main(String[] args)

Jobs to be done:
1.Create a package
2.Create a class named Traverse
3.Create a list named arr and add elements to it.
4.Create an empty linked list named list 
5.Add elements of arr to list
6.using hasNext() function traverse the elements of arr and print the result.
 */

package com.java.training.core.collections;

import java.util.List;
import java.util.ArrayList;
import java.util.*;
import java.util.Iterator;

public class Traverse {

	public static void main(String[] args) {
		List<Integer> arr=new ArrayList<>();
		arr.add(10);
		arr.add(20);
		arr.add(30);
		arr.add(40);
		arr.add(50);
		arr.add(60);
		arr.add(70);
		System.out.println(" "+arr);
		LinkedList<Integer> list=new LinkedList<>();
		list.addAll(arr);
		System.out.println(" "+list);
		Iterator i=list.iterator();
		while(i.hasNext()) {
			System.out.println(i.next());
		}
	}

}
