/*
 Requirement:
 To Count the size of mappings in a map.
 
 Entities:
 class named Size is used.
 
 Function Declaration:
 No function is declared in this program.
 
 Jobs to be done:
 1.Create a package
 2.Import util HashMap and Map packages
 3.create a class named Size
 4.create a map and put the elements into it.
 5.Using size() function print the size of the map.
 */




package com.java.training.core.Map;
import java.util.HashMap;
import java.util.Map;

public class Size {

	public static void main(String[] args) {
		Map<String,String> m=new HashMap<String,String>();
		m.put("1", "One");
		m.put("2","Two");
		m.put("3","Three");
		System.out.println(" "+m);
		System.out.println("Size of elements :" +m.size());

	}

}
