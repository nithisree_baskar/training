/*
Requirements:
Consider a following code snippet
        Queue bike = new PriorityQueue();    
        bike.poll();
        System.out.println(bike.peek());    
what will be output and complete the code.
Entities:
The class named as Bike
FunctionDeclaration:
  public static void main(String[] args)
jobs to be done:
1.create and import a package
2.create a class called Bike
3.create a Queue with generic String type and adding values using add() method.
4.use poll() method ,to remove front element and using peek() printing the peek value.
5.print the result. 
 */




package com.java.training.core.stackandqueue;
import java.util.*;
public class Bike {

	public static void main(String[] args) {
		Queue<String> bike=new PriorityQueue<String>();
		bike.add("Royal Enfield");
		bike.add("RX 100");
		bike.add("Duke");
		bike.add("Pulsar");
		bike.poll();
		System.out.println(bike.peek());

	}

}
