/*
Requirement:
To add and remove the elements in stack

Entities:
A class named StackAdd

Function Declaration:
public static void main(String[] args)

Jobs to be done:
1.create a stack
2.add elements using add() function
3.print the stack elements
4.using remove() function remove a element from stack and print the result.
 */



package com.java.training.core.collections;
import java.util.Stack;
public class StackAdd {

	public static void main(String[] args) {
		Stack<String> s=new Stack<String>();
		s.add("This");
		s.add("is");
		s.add("the");
		s.add("sample");
		s.add("java");
		s.add("code");
		System.out.println("The Stack is "+s);
		String a=s.remove(3);
		System.out.println(a);
		System.out.println(" The Stack after removing "+s);

	}

}
