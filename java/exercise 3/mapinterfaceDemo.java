/*
Requirement:
To demonstrate java program to working of map interface using( put(), remove(), booleanContainsValue(), replace() ) .

Entities:
A class named mapinterfaceDemo is used.

Function Declaration:
public static void main(String[] args) 

Jobs to be done:
1.Create a package
2.Create a Hashmap named map and add elements to it using put() method.
3.Display the map
4.using remove() method remove an element and print the removed element.
5.Using containsValue() method check if a value is present in the map and print the result.
6.using replace() method replace an exisiting value with new value and print the entire map.
 
 */




package com.java.training.core.setsandmaps;
import java.util.*;

public class mapinterfaceDemo {

	public static void main(String[] args) {
	Map<Integer,String> map=new HashMap<Integer,String>();
	map.put(1, "Nithi");
	map.put(2, "Iniya");
	map.put(3,"Dharshanaa");
	map.put(4, "Mithun");
	map.put(5, "Vicas");
	map.put(6, "Harini");
	for(Map.Entry m:map.entrySet()) {
		System.out.println(m.getKey()+" "+m.getValue());
	}
    String r=map.remove(4);
    System.out.println(" Returned value is "+ r);
    System.out.println("Is Nithi present in the map? "+ map.containsValue("Nithi"));
    System.out.println("Is Mithun present in the map? "+ map.containsValue("Mithun"));
    map.replace(5,"Srivicas");
    System.out.println("New Hashmap: " +map);
	}

}
