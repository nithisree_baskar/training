/*
Requirement:
To write a code for sorting vector list in descending order.

Entities:
class named VectorList is used.

Function Declaration:
public static void main(String[] args)

Jobs to be done:
1.Create a class named VectorList
2.Create a vector and add elements to it.
3.Print the vector before sorting
4.Using Collections.sort() function sort the vector and print the result.
 */


package com.java.training.core.collections;
import java.util.Collections;
import java.util.Vector;

public class VectorList {

	public static void main(String[] args) {
		Vector<String> vectorlist=new Vector<String>();
		vectorlist.add("Nithi");
		vectorlist.add("Iniya");
		vectorlist.add("Dharshanaa");
		vectorlist.add("Annie");
		vectorlist.add("Mithun");
		vectorlist.add("Vicas");
		System.out.println("Vector before Sorting : ");
		for(int i=0;i<vectorlist.size();i++) {
			System.out.println(vectorlist.get(i));
		}
		Collections.sort(vectorlist);
		System.out.println("Vector List after Sorting : ");
		for(int i=0;i<vectorlist.size();i++) {
			System.out.println(vectorlist.get(i));
		}

	}

}
