/*
Requirements:
Create a queue using generic type and in both implementation Priority Queue,Linked list and complete following  
  -> add atleast 5 elements
  -> remove the front element
  -> search a element in stack using contains key word and print boolean value value
  -> print the size of stack
  -> print the elements using Stream
Entities:
The class named as UsingQueue

FunctionDeclaration:
public void linkedList()
public void priority()
public static void main(String[] args)

Jobs to be done:
1.create a package and Import a package
2.Create a class called UsingQueue,create a function called linkedList() and invoking two methods
3.In a linkedList() method creating queue and add elements in that.
4.use size() method, to find the size of the elements and use poll() method ,to remove the front element.
5.Use contains() to check the specific element in that queue and Printing the queue values using stream and for each value.
6.In a priority() method creating queue and add() method is used to adding the values.
7.Use size()method,to print size of element in queue and poll() used to remove the front element.
8.For finding specified value using contains() method and printing the PriorityQueue values using stream and for each
 */




package com.java.training.core.stackandqueue;
import java.util.Queue;
import java.util.stream.Stream;
import java.util.LinkedList;
import java.util.PriorityQueue;


public class UsingQueue {
	public void linkedlist() {
		Queue<Integer> q=new LinkedList<Integer>();
		q.add(1);
		q.add(2);
		q.add(3);
		q.add(4);
		q.add(5);
		System.out.println(" "+q);
		int e=q.size();
		System.out.println("Size :"+e);
		int a=q.poll();
		System.out.println(a);
		System.out.println(q.contains(10));
		Stream m = q.stream();
	    m.forEach((element) -> {
		System.out.println(element);  
	});
	}
	
	public void Priority(){
		Queue<Integer> p=new PriorityQueue<Integer>();
		p.add(10);
		p.add(20);
		p.add(30);
		p.add(40);
		p.add(50);
		System.out.println(""+p);
		int h=p.size();
		System.out.println("Size "+h);
		int b=p.poll();
		System.out.println(b);
		System.out.println(p);
		Stream z = p.stream();
	    z.forEach((element) -> {
		System.out.println(element);  
			});
		
	}

	public static void main(String[] args) {
		UsingQueue queue=new UsingQueue();
		queue.linkedlist();
		queue.Priority();

	}

}
