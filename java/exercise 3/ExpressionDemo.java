/*
Requirement:
To perform Addition,Substraction,Multiplication and Division concepts using Lambda expression and functional interface

Entities:
class named ExpressionDemo

Function Declaration:
public static void main(String[] args)
operation()

Jobs to be done:
1.Use the package collections
2.Create a function arithmetic
3.create a class named ExpressionDemo
4.call the function arithmetic for performing addition,subtraction,multiplication,division
5.print the result
 */




package com.java.training.core.collections;

interface Arithmetic{
	int operation(int a,int b);
}
public class ExpressionDemo {

	public static void main(String[] args) {
		Arithmetic addition=(int a,int b)->(a+b);
		System.out.println("Addition "+addition.operation(4, 5));
		Arithmetic subtraction=(int a,int b)->(a-b);
		System.out.println("Subtraction "+subtraction.operation(5, 4));
		Arithmetic multiplication=(int a,int b)->(a*b);
		System.out.println("Multiplication "+multiplication.operation(10, 10));
		Arithmetic division=(int a,int b)->(a/b);
		System.out.println("Division "+division.operation(50, 25));

	}

}
