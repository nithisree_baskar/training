
Requirement:
   To assume you have written some classes. Belatedly, you decide they should be split into three packages, as listed in the following table.
  Furthermore, assume the classes are currently in the default package (they have no package statements).
        Package Name    Class Name
        mygame.server   Server
        mygame.shared   Utilities
        mygame.client   Client

Entities:
   No class is used.

Function Declaration:
   No function is declared.

Solution:
a. What line of code will you need to add to each source file to put each class in the right package?
Ans: The first line of each file must specify the package:

In Client.java add:
package mygame.client;
In Server.java add:
package mygame.server;:
In Utilities.java add:
package mygame.shared;

		