
Requirement:
   To consider the following two classes and answer the following.
   
Entities:
  ClassA and ClassB classes are used.

Function Declaration:
  methodOne, methodTwo, methodThree,methodFour functions are declared in this program.

Solution:

Consider the following two classes:

public class ClassA {
    public void methodOne(int i) {
    }
    public void methodTwo(int i) {
    }
    public static void methodThree(int i) {
    }
    public static void methodFour(int i) {
    }
}

public class ClassB extends ClassA {
    public static void methodOne(int i) {
    }
    public void methodTwo(int i) {
    }
    public void methodThree(int i) {
    }
    public static void methodFour(int i) {
    }
}
Question: Which method overrides a method in the superclass?
Ans: methodTwo

Question: Which method hides a method in the superclass?
Ans: methodFour

Question: What do the other methods do?
Ans: They cause compile-time errors.