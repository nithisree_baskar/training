
/*
Requirement:
       To create a class named Dog and define a function named sound.

Entities:
    class named Dog is used.

Function Declaration:
    sound function is declared in this program.

Jobs to be done:
    1. Declare the class Dog.
    2. call the method sound().
    3. Print the the statement as given.
*/

Solution:

public class Dog extends Animal1 {
    public void sound() {
        System.out.println("the dog barks");
    }
}