
/*
Requirement:
    To create a class named Cat and define a function named sound.
Entities:
    Class named Cat is used in this program.

Function Declaration:
    sound function is declared in this program.

Jobs to be done:
    1. Declare the class Cat.
    2. call the method sound().
    3. Print the the statement as given.
*/

Solution:

public class Cat extends Animal1 {
    public void sound() {
        System.out.println("the cat meows");
    }
}