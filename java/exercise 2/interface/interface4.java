
/*Requirement:
  To write a class that implements the CharSequence interface found in the java.lang package.
  Your implementation should return the string backwards. Write a small main method to test your class; make sure to call all four methods.

Entities:
  class IteratorDemo is used.

Function Declaration:
  hasNext,next and remove functions are used

Job to be done:
  1. Declare a class IteratorDemo.
  2. Initialize String CardNames
  3. define functions hasNext,next and remove
  4. In the main function call the functions */



public class IteratorDemo implements java.util.Iterator {
    private String[] cardNames = {
             "2", "3", "4", "5", "6", "7", "8", "9",
	     "10", "Jack", "Queen", "King", "Ace" };
    private int current = 0;
    
    public boolean hasNext() {
        if (current == cardNames.length) {
            return false;
        } else {
            return true;
        }
    }
    public Object next() {
        return (Object)cardNames[current++];
    }
    public void remove() {
        throw new UnsupportedOperationException();
    }

    public static void main(String[] args) {
        IteratorDemo i = new IteratorDemo();
        while (i.hasNext()) {
            System.out.println(i.next());
        }
    }
}