
Requirement:
   To find what is wrong with the following interface and fix it.
    public interface SomethingIsWrong {
        void aMethod(int aValue){
            System.out.println("Hi Mom");
        }

Entities:
   SomethingIsWrong class is used

Function Declaration:
   No function is declared in this program

Solution:
   It has a method implementation in it. Only default and static methods have implementations.
Fix the code:
  public interface SomethingIsWrong {
      void aMethod(int aValue);
	  }
	  
    