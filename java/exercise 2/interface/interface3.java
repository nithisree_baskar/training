
Requirement:
   To check whether the following interface is valid

Entities:
   no class is used

Function Declaration:
   no function is declared

Solution:
Yes,the interface is valid. Methods are not required. 
Empty interfaces can be used as types and to mark classes without requiring any particular method implementations. 
