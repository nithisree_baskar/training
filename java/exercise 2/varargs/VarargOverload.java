
/*Requirement:
  To demonstrate overloading with varArgs

Entities:
  class VarargOverload is used in this program.

Function Declaration:
  test function is used in this program.

Job to be done:
  1. Create a class named VarargOverload.
  2. create a function named test with different parameters
  3. call the functions in the main.*/
 
class VarargOverload {

    private void test(int ... args){
        int sum = 0;
        for (int i: args) {
            sum += i;
        }
        System.out.println("sum = " + sum);
    }

    private void test(boolean p, String ... args){
        boolean negate = !p;
        System.out.println("negate = " + negate);
        System.out.println("args.length = "+ args.length);
    }

    public static void main( String[] args ) {
        VarargOverload obj = new VarargOverload();
        obj.test(1, 2, 3);
        obj.test(true, "hello", "world");
    }
}
