
/*Requirement:
  To sort and print following String[] alphabetically ignoring case. Also convert and print even indexed Strings into uppercase
       { "Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" }
	   
Entities:
   class named Sort is used.

Function Declaration:
   No function is declared in this program.

Job to be done:
   1. import Array function
   2. create a public class Sort
   3. initialize the array
   4. using for loop arrange the array in alphabetic order.
   5. print the result*/
   
Solution:  
 
import java.util.Arrays; 
public class Sort{
    public static void main(String[] args){
        String [] place = {"Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" };
        Arrays.sort(place);
        System.out.println(Arrays.toString(place));
        for(int i=0;i<place.length;i++)
        {
            if(i%2==0) {
                place[i] = place[i].toUpperCase();
            }
        }
        System.out.print(Arrays.toString(place));
    }
}