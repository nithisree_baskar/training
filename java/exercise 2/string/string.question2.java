
Requirement:
   The following code creates one array and one string object. How many references to those objects exist after the code executes? Is either object eligible for garbage collection?
    
    String[] students = new String[10];
    String studentName = "Peter Parker";
    students[0] = studentName;
    studentName = null;
	
Entities:
   No class is used in this program

Function Declaration:
   No function declaration is declared in this program

Job to be done:
   1. Read the code
   2. Find the references to those objects that exist after the code executes
   3. Write the solution

Solution:
  There is one reference to the students array and that array has one reference to the string Peter Smith. Neither object is eligible for garbage collection. 
  The array students is not eligible for garbage collection because it has one reference to the object studentName even though that object has been assigned the value null.
  The object studentName is not eligible either because students[0] still refers to it.
