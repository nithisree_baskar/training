
/*Requirement:
   To write a program that computes your initials from your full name and displays them.

Entities:
   class named ComputeInitials is used in this program.

Function Declaration:
   No function is declared in this program.

Job to be done:
   1. import Scanner function.
   2. In the main function declare myName and get the string in myName.
   3. find the length of the string
   4. check for uppercase in the string and append that character in myInitials.
   5. display the myInitials.*/

Solution:
   
import java.util.Scanner;
public class ComputeInitials {
    public static void main(String[] args) {
        String myName;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a name");
		myName=sc.nextLine();
        StringBuffer myInitials = new StringBuffer();
        int length = myName.length();

        for (int i = 0; i < length; i++) {
            if (Character.isUpperCase(myName.charAt(i))) {
                myInitials.append(myName.charAt(i));
            }
        }
        System.out.println("initials are: " + myInitials);
    }
}