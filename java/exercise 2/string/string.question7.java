
Requirement:
  To show two ways to concatenate the following two strings together to get the string "Hi, mom.":
    String hi = "Hi, ";
    String mom = "mom.";

Entities:
  No class is used in this program.

Function Declaration:
  No function is declared in this program.

Solution:
  hi.concat(mom) and hi + mom.