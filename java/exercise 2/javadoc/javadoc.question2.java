
Requirement:
  To find what methods would a class that implements the java.lang.CharSequence interface have to implement.

Entities:
  No class is used.

Function Declaration: 
  No function is declared in this program.  
  
Solution:  
What methods would a class that implements the java.lang.CharSequence interface have to implement?
Ans: charAt, length, subSequence, and toString.

