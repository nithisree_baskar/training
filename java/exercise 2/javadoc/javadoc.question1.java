
Requirement:
   Use the Java API documentation for the Box class (in the javax.swing package) to help you answer the following questions.

Entities:
    No class is used

Function Declaration:
    No function is declared in this program

Solution:

What static nested class does Box define?
Ans: Box.Filler
   
What inner class does Box define?
Ans: Box.AccessibleBox

What is the superclass of Box's inner class?
Ans: [java.awt.]Container.AccessibleAWTContainer

Which of Box's nested classes can you use from any class?
Ans: Box.Filler

How do you create an instance of Box's Filler class?
Ans: new Box.Filler(minDimension, prefDimension, maxDimension)