
Requirement:
  To find what Integer method can you use to convert an int into a string that expresses the number in hexadecimal? For example, what method converts the integer 65 into the string "41"?

Entities:
  No class is used

Function Declaration:
  No function is declared

Solution:
What Integer method can you use to convert an int into a string that expresses the number in hexadecimal? For example, what method converts the integer 65 into the string "41"?

Ans: toHexString